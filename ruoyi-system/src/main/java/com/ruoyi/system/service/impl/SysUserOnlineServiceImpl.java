/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:SysUserOnlineServiceImpl.java
 * Date:2020/09/13 08:43:13
 */

package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.SysUserOnline;
import com.ruoyi.system.service.ISysUserOnlineService;
import org.springframework.stereotype.Service;

/**
 * 在线用户 服务层处理
 *
 * @author ruoyi
 */
@Service
public class SysUserOnlineServiceImpl implements ISysUserOnlineService {
    /**
     * 通过登录地址查询信息
     *
     * @param ipaddr 登录地址
     * @param user   用户信息
     * @return 在线用户信息
     */
    @Override
    public SysUserOnline selectOnlineByIpaddr(String ipaddr, SysUser user) {
        if (StringUtils.equals(ipaddr, user.getLoginIp())) {
            return loginUserToUserOnline(user);
        }
        return null;
    }

    /**
     * 通过用户名称查询信息
     *
     * @param userName 用户名称
     * @param user     用户信息
     * @return 在线用户信息
     */
    @Override
    public SysUserOnline selectOnlineByUserName(String userName, SysUser user) {
        if (StringUtils.equals(userName, user.getUserName())) {
            return loginUserToUserOnline(user);
        }
        return null;
    }

    /**
     * 通过登录地址/用户名称查询信息
     *
     * @param ipaddr   登录地址
     * @param userName 用户名称
     * @param user     用户信息
     * @return 在线用户信息
     */
    @Override
    public SysUserOnline selectOnlineByInfo(String ipaddr, String userName, SysUser user) {
        if (StringUtils.equals(ipaddr, user.getLoginIp()) && StringUtils.equals(userName, user.getUserName())) {
            return loginUserToUserOnline(user);
        }
        return null;
    }

    /**
     * 设置在线用户信息
     *
     * @param user 用户信息
     * @return 在线用户
     */
    @Override
    public SysUserOnline loginUserToUserOnline(SysUser user) {
        if (StringUtils.isNull(user)) {
            return null;
        }
        SysUserOnline sysUserOnline = new SysUserOnline();
        //sysUserOnline.setTokenId(user.getToken());
        sysUserOnline.setUserName(user.getUserName());
        sysUserOnline.setIpaddr(user.getLoginIp());
        // sysUserOnline.setLoginLocation(user.getLoginLocation());
        // sysUserOnline.setBrowser(user.getBrowser());
        // sysUserOnline.setOs(user.getOs());
     //   sysUserOnline.setLoginTime(user.getLoginDate().getTime());
        if (StringUtils.isNotNull(user.getDept())) {
            sysUserOnline.setDeptName(user.getDept().getDeptName());
        }
        return sysUserOnline;
    }
}
