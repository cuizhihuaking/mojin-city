/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:ISysStoreUserOnlineService.java
 * Date:2020/09/13 08:43:13
 */

package com.ruoyi.system.service;


import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.system.domain.SysUserOnline;

/**
 * 在线用户 服务层
 *
 * @author ruoyi
 */
public interface ISysStoreUserOnlineService {
    /**
     * 通过登录地址查询信息
     *
     * @param ipaddr 登录地址
     * @param user   用户信息
     * @return 在线用户信息
     */
    public SysUserOnline selectOnlineByIpaddr(String ipaddr, SysUser user);

    /**
     * 通过用户名称查询信息
     *
     * @param userName 用户名称
     * @param user     用户信息
     * @return 在线用户信息
     */
    public SysUserOnline selectOnlineByUserName(String userName, SysUser user);

    /**
     * 通过登录地址/用户名称查询信息
     *
     * @param ipaddr   登录地址
     * @param userName 用户名称
     * @param user     用户信息
     * @return 在线用户信息
     */
    public SysUserOnline selectOnlineByInfo(String ipaddr, String userName, SysUser user);

    /**
     * 设置在线用户信息
     *
     * @param user 用户信息
     * @return 在线用户
     */
    public SysUserOnline loginUserToUserOnline(SysUser user);
}
