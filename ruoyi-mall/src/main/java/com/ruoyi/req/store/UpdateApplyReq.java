package com.ruoyi.req.store;

import lombok.Getter;
import lombok.Setter;

/**
 * @Description: 修改门店申请
 * @Author: Jayvin Leung
 * @Date: 2019/9/12 13:51
 */
@Getter
@Setter
public class UpdateApplyReq extends ApplyStoreReq {
    public static final String URI = "/store/updateApply.do";
    private Long id;

    @Override
    public String toString() {
        return "UpdateApplyReq{" +
                "id=" + id +
                '}';
    }
}
