package com.ruoyi.req.store;

import com.ruoyi.req.base.PageReq;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 门店中心 -- 我的客户
 * @Author: Jayvin Leung
 * @Date: 2019/9/10 12:32
 */
@Getter
@Setter
public class StoreClientListReq extends PageReq implements Serializable {
    public static final String URI = "/store/getClientList.do";
    private Long storeId;

    @Override
    public String toString() {
        return "StoreClientListReq{" +
                "storeId=" + storeId +
                '}';
    }
}
