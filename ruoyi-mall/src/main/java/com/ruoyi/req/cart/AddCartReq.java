package com.ruoyi.req.cart;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 加购物车
 * @Author: Jayvin Leung
 * @Date: 2019/9/5 9:34
 */
@Getter
@Setter
public class AddCartReq implements Serializable {
    public static final String URI = "/cart/add.do";
    private int platformFlag = 2;
    private Long storeId;
    private Long goodsId;
    private Long skuId;
    private Integer num;
}
