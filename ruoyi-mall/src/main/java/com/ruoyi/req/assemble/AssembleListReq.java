package com.ruoyi.req.assemble;


import com.ruoyi.req.base.PageReq;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 拼团列表
 * @Author: Jayvin Leung
 * @Date: 2019/9/4 20:19
 */
@Getter
@Setter
public class AssembleListReq extends PageReq implements Serializable {
    public static final String URI = "/assemble/getAssembleList.do";
    private int platformFlag = 2;
    private Long storeId;

    @Override
    public String toString() {
        return "AssembleListReq{" +
                "platformFlag=" + platformFlag +
                ", storeId=" + storeId +
                '}';
    }
}
