package com.ruoyi.req.assemble;

import com.ruoyi.req.base.PageReq;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 更多拼团
 * @Author: Jayvin Leung
 * @Date: 2019/9/4 20:19
 */
@Getter
@Setter
public class UserAssembleListReq extends PageReq implements Serializable {
    public static final String URI = "/assemble/getUserAssembleList.do";
    private Long activeId;
    private Long goodsId;

    @Override
    public String toString() {
        return "UserAssembleListReq{" +
                "activeId=" + activeId +
                '}';
    }
}
