package com.ruoyi.req;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 二维码
 * @Author: Jayvin Leung
 * @Date: 2019/9/4 13:14
 */
@Getter
@Setter
public class ShareQRReq implements Serializable {
    public static final String URI = "/common/getShareQR.do";
    private String scene;
    private String page;

    @Override
    public String toString() {
        return "ShareQRReq{" +
                "scene='" + scene + '\'' +
                ", page='" + page + '\'' +
                '}';
    }
}
