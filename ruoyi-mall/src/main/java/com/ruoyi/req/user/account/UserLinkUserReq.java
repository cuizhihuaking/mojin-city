package com.ruoyi.req.user.account;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 用户 邀请 用户 统一处理接口
 * @Author: Jayvin Leung
 * @Date: 2019/9/3 16:17
 */
@Getter
@Setter
public class UserLinkUserReq implements Serializable {
    public static final String URI = "/account/userLinkUser.do";
    private String promoCode; // 邀请码，必填
    private Long userId; // 被邀请人id，选填
    private String jsCode; // 被邀请人jsCode，选填

    @Override
    public String toString() {
        return "UserLinkUserReq{" +
                "promoCode='" + promoCode + '\'' +
                ", userId=" + userId +
                ", jsCode='" + jsCode + '\'' +
                '}';
    }
}
