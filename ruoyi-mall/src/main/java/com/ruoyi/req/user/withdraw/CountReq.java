package com.ruoyi.req.user.withdraw;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description: 计算提现手续费以及实际金额
 * @Author: Jayvin Leung
 * @Date: 2019/9/9 23:20
 */
@Getter
@Setter
public class CountReq implements Serializable {
    public static final String URI = "/withdraw/count.do";
    private BigDecimal money = new BigDecimal(0);

    @Override
    public String toString() {
        return "CountReq{" +
                "money=" + money +
                '}';
    }
}
