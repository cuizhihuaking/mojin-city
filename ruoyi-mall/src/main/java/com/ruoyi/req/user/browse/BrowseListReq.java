package com.ruoyi.req.user.browse;

import com.ruoyi.req.base.PageReq;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 浏览记录列表
 * @Author: Jayvin Leung
 * @Date: 2019/10/29 0:54
 */
@Getter
@Setter
public class BrowseListReq extends PageReq implements Serializable {
    public static final String URI = "/browse/getRecordList.do";
    private Long userId;
    private Long date;

    @Override
    public String toString() {
        return "BrowseListReq{" +
                "userId=" + userId +
                ", date=" + date +
                '}';
    }
}
