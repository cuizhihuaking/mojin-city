package com.ruoyi.req.user.distribution;

import com.ruoyi.req.base.PageReq;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 客户列表
 * @Author: Jayvin Leung
 * @Date: 2019/9/9 21:41
 */
@Getter
@Setter
public class DistributionClientListReq extends PageReq implements Serializable {
    public static final String URI = "/distribution/getClientList.do";
    private int level = 1;
    private String memberId;

    @Override
    public String toString() {
        return "DistributionClientListReq{" +
                "level=" + level +
                '}';
    }
}
