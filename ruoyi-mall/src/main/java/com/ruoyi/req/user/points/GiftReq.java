package com.ruoyi.req.user.points;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 赠送福气
 * @Author: Jayvin Leung
 * @Date: 2019/9/8 23:08
 */
@Getter
@Setter
public class GiftReq implements Serializable {
    public static final String URI = "/points/gift.do";
    private int points;
    private Long memberId;

    @Override
    public String toString() {
        return "GiftReq{" +
                "points=" + points +
                ", memberId=" + memberId +
                '}';
    }
}
