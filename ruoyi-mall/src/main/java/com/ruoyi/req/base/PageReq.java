package com.ruoyi.req.base;

import java.io.Serializable;

public class PageReq implements Serializable {

    private int page = 1;
    private int pageSize = 15;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "PageReq{" +
                "page=" + page +
                ", pageSize=" + pageSize +
                '}';
    }
}
