package com.ruoyi.req.home;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 选择预绑定门店
 * @Author: Jayvin Leung
 * @Date: 2019/9/3 15:04
 */
@Getter
@Setter
public class SelectStoreReq implements Serializable {
    public static final String URI = "/home/selectStore.do";
    private Long selectStoreId;
    private String lon;
    private String lat;

    @Override
    public String toString() {
        return "SelectStoreReq{" +
                "selectStoreId=" + selectStoreId +
                ", lon='" + lon + '\'' +
                ", lat='" + lat + '\'' +
                '}';
    }
}
