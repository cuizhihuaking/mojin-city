package com.ruoyi.req.goods;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 商品详情
 * @Author: Jayvin Leung
 * @Date: 2019/9/4 15:51
 */
@Getter
@Setter
public class GoodsDetailReq implements Serializable {
    public static final String URI = "/goods/getGoodsDetail.do";
    private Long id;
    private Long thirdId;
    private Long storeId;

    @Override
    public String toString() {
        return "GoodsDetailReq{" +
                "id=" + id +
                ", thirdId=" + thirdId +
                ", storeId=" + storeId +
                '}';
    }
}
