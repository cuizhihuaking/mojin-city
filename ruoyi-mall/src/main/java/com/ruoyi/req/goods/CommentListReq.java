package com.ruoyi.req.goods;

import com.ruoyi.req.base.PageReq;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 商品评论
 * @Author: Jayvin Leung
 * @Date: 2019/9/4 15:38
 */
@Getter
@Setter
public class CommentListReq extends PageReq implements Serializable {
    public static final String URI = "/goods/getCommentList.do";
    private Long goodsId;

    @Override
    public String toString() {
        return "CommentListReq{" +
                "goodsId=" + goodsId +
                '}';
    }
}
