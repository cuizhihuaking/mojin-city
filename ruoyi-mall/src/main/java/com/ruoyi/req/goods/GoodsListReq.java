package com.ruoyi.req.goods;

import com.ruoyi.req.base.PageReq;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 商品搜索
 * @Author: Jayvin Leung
 * @Date: 2019/9/2 14:04
 */
@Getter
@Setter
public class GoodsListReq extends PageReq implements Serializable {
    public static final String URI = "/goods/getGoodsList.do";
    private Long brandId; // 品牌id
    private Long classifyId; // 分类id
    private String keyword; // 关键字
    private Integer priceSort; // 价格排序
    private Integer soldNumSort; // 销量排序
    private String goodsIds; // 商品ids
    private int platformFlag = 2; // 平台标识
    private Long storeId;
    private Integer type = 1;//1.普通情况 2.其他情况（权益卡）

    @Override
    public String toString() {
        return "GoodsListReq{" +
                "brandId=" + brandId +
                ", classifyId=" + classifyId +
                ", keyword='" + keyword + '\'' +
                ", priceSort=" + priceSort +
                ", soldNumSort=" + soldNumSort +
                ", platformFlag=" + platformFlag +
                ", storeId=" + storeId +
                '}';
    }
}
