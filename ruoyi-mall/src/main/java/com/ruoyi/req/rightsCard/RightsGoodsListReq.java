package com.ruoyi.req.rightsCard;

import com.ruoyi.req.base.PageReq;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 权益专场商品列表
 * @Author: Jayvin Leung
 * @Date: 2019/10/22 16:54
 */
@Getter
@Setter
public class RightsGoodsListReq extends PageReq implements Serializable {
    public static final String URI = "/rightsCard/getGoodsList.do";
    private int platformFlag = 2; // 平台标识
    private Long storeId;

    @Override
    public String toString() {
        return "RightsGoodsListReq{" +
                "platformFlag=" + platformFlag +
                ", storeId=" + storeId +
                '}';
    }
}
