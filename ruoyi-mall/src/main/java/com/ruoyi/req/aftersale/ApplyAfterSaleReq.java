package com.ruoyi.req.aftersale;

import lombok.Data;

/**
 * @Description:
 * @Author: liuweicheng
 * @Date: 2019-10-11 14:56
 */
@SuppressWarnings("all")
@Data
public class ApplyAfterSaleReq {

    private String orderItemIds;//订单项ids

    private Integer type;//售后类型

    private Integer status;//货物状态

    private String reason; //申请原因

    private String imgUrl;//图片凭证

    private String description;//说明

}
