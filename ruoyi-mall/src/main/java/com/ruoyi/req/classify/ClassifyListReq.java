package com.ruoyi.req.classify;

import com.ruoyi.req.base.PageReq;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 商品分类
 * @Author: Jayvin Leung
 * @Date: 2019/9/2 14:04
 */
@Getter
@Setter
public class ClassifyListReq extends PageReq implements Serializable {
    public static final String URI = "/classify/getClassifyList.do";
    private int level;
    private Long parentId;
    private int platformFlag = 2;
    private Long storeId;
    private int isRecommend; // 是否推荐榜单

    @Override
    public String toString() {
        return "ClassifyListReq{" +
                "level=" + level +
                ", parentId=" + parentId +
                ", platformFlag=" + platformFlag +
                ", storeId=" + storeId +
                ", isRecommend=" + isRecommend +
                '}';
    }
}
