package com.ruoyi.tc.service.impl;


import com.ruoyi.tc.domain.TcJobClass;
import com.ruoyi.tc.mapper.TcJobClassMapper;
import com.ruoyi.tc.service.ITcJobClassService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 职位分类Service业务层处理
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Slf4j
@Service
public class TcJobClassServiceImpl implements ITcJobClassService {

    @Autowired
    private TcJobClassMapper tcJobClassMapper;

    /**
     * 查询职位分类
     *
     * @param id 职位分类ID
     * @return 职位分类
     */
    @Override
    public TcJobClass selectTcJobClassById(Long id) {
        return tcJobClassMapper.selectTcJobClassById(id);
    }

    /**
     * 查询职位分类列表
     *
     * @param tcJobClass 职位分类
     * @return 职位分类
     */
    @Override
    public List<TcJobClass> selectTcJobClassList(TcJobClass tcJobClass) {
        return tcJobClassMapper.selectTcJobClassList(tcJobClass);
    }

    /**
     * 新增职位分类
     *
     * @param tcJobClass 职位分类
     * @return 结果
     */
    @Override
    public int insertTcJobClass(TcJobClass tcJobClass) {
        return tcJobClassMapper.insertTcJobClass(tcJobClass);
    }

    /**
     * 修改职位分类
     *
     * @param tcJobClass 职位分类
     * @return 结果
     */
    @Override
    public int updateTcJobClass(TcJobClass tcJobClass) {
        return tcJobClassMapper.updateTcJobClass(tcJobClass);
    }

    /**
     * 批量删除职位分类
     *
     * @param ids 需要删除的职位分类ID
     * @return 结果
     */
    @Override
    public int deleteTcJobClassByIds(Long[] ids) {
        return tcJobClassMapper.deleteTcJobClassByIds(ids);
    }

    /**
     * 删除职位分类信息
     *
     * @param id 职位分类ID
     * @return 结果
     */
    @Override
    public int deleteTcJobClassById(Long id) {
        return tcJobClassMapper.deleteTcJobClassById(id);
    }

    @Override
    public List<TcJobClass> queryAllFirstAndSecondCategory(TcJobClass tagParam) {
        List<TcJobClass> allList = tcJobClassMapper.selectTcJobClassList(tagParam);
        List<TcJobClass> parentList= allList.stream().filter(TcJobClass::isFirstCategory).collect(Collectors.toList());
        for (TcJobClass  jobClass:parentList){
            for (TcJobClass  aJb:allList) {
                if (jobClass.getId()==aJb.getParentCode()) {
                    jobClass.getChildren().add(aJb);
                }
            }
        }
        return parentList;
    }
}
