package com.ruoyi.tc.service;

import com.ruoyi.tc.domain.TcTuiguang;

import java.util.List;

/**
 * 推广Service接口
 *
 * @author é­éåå
 * @date 2022-01-21
 */
public interface ITcTuiguangService {
    /**
     * 查询推广
     *
     * @param id 推广ID
     * @return 推广
     */
    public TcTuiguang selectTcTuiguangById(Long id);

    /**
     * 查询推广列表
     *
     * @param tcTuiguang 推广
     * @return 推广集合
     */
    public List<TcTuiguang> selectTcTuiguangList(TcTuiguang tcTuiguang);

    /**
     * 新增推广
     *
     * @param tcTuiguang 推广
     * @return 结果
     */
    public int insertTcTuiguang(TcTuiguang tcTuiguang);

    /**
     * 修改推广
     *
     * @param tcTuiguang 推广
     * @return 结果
     */
    public int updateTcTuiguang(TcTuiguang tcTuiguang);

    /**
     * 批量删除推广
     *
     * @param ids 需要删除的推广ID
     * @return 结果
     */
    public int deleteTcTuiguangByIds(Long[] ids);

    /**
     * 删除推广信息
     *
     * @param id 推广ID
     * @return 结果
     */
    public int deleteTcTuiguangById(Long id);

    /**
     * 当月发布数
     * @param userId
     * @return
     */
    Long countByMonth(Long userId);
}
