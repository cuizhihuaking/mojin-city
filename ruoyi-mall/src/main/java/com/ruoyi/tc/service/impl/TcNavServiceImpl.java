package com.ruoyi.tc.service.impl;


import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.tc.domain.TcNav;
import com.ruoyi.tc.mapper.TcNavMapper;
import com.ruoyi.tc.service.ITcNavService;
import com.ruoyi.tc.vo.TcTcNavVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 模块Service业务层处理
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Slf4j
@Service
public class TcNavServiceImpl implements ITcNavService {

    @Autowired
    private TcNavMapper tcNavMapper;

    /**
     * 查询模块
     *
     * @param id 模块ID
     * @return 模块
     */
    @Override
    public TcNav selectTcNavById(Long id) {
        return tcNavMapper.selectTcNavById(id);
    }

    /**
     * 查询模块列表
     *
     * @param tcNav 模块
     * @return 模块
     */
    @Override
    public List<TcNav> selectTcNavList(TcNav tcNav) {
        return tcNavMapper.selectTcNavList(tcNav);
    }

    /**
     * 新增模块
     *
     * @param tcNav 模块
     * @return 结果
     */
    @Override
    public int insertTcNav(TcNav tcNav) {
        return tcNavMapper.insertTcNav(tcNav);
    }

    /**
     * 修改模块
     *
     * @param tcNav 模块
     * @return 结果
     */
    @Override
    public int updateTcNav(TcNav tcNav) {
        return tcNavMapper.updateTcNav(tcNav);
    }

    /**
     * 批量删除模块
     *
     * @param ids 需要删除的模块ID
     * @return 结果
     */
    @Override
    public int deleteTcNavByIds(Long[] ids) {
        return tcNavMapper.deleteTcNavByIds(ids);
    }

    /**
     * 删除模块信息
     *
     * @param id 模块ID
     * @return 结果
     */
    @Override
    public int deleteTcNavById(Long id) {
        return tcNavMapper.deleteTcNavById(id);
    }
    @Override
    public List<TcTcNavVo> queryAllFirstAndSecondCategory(TcNav tagParam) {
        List<TcNav> allList = tcNavMapper.selectTcNavList(tagParam);
        List<TcTcNavVo> parentList= new ArrayList<>();
        List<TcNav> tempList = allList.stream().filter(TcNav::isFirstCategory).collect(Collectors.toList());
        for (TcNav obj: tempList){
            TcTcNavVo targetObj = new TcTcNavVo();
            BeanUtils.copyProperties(obj, targetObj);
            parentList.add(targetObj);
        }
        for (TcTcNavVo jobClass:parentList){
            for (TcNav  aJb:allList) {
                if (jobClass.getId()==aJb.getPid()) {
                    jobClass.getChildren().add(aJb);
                }
            }
        }
        return parentList;
    }
}
