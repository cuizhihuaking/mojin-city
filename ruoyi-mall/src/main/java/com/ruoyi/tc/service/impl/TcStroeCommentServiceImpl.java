package com.ruoyi.tc.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.tc.domain.TcStroeComment;
import com.ruoyi.tc.mapper.TcStroeCommentMapper;
import com.ruoyi.tc.service.ITcStroeCommentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Slf4j
@Service
public class TcStroeCommentServiceImpl implements ITcStroeCommentService {

    @Autowired
    private TcStroeCommentMapper tcStroeCommentMapper;

    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public TcStroeComment selectTcStroeCommentById(Long id) {
        return tcStroeCommentMapper.selectTcStroeCommentById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     *
     * @param tcStroeComment 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<TcStroeComment> selectTcStroeCommentList(TcStroeComment tcStroeComment) {
        return tcStroeCommentMapper.selectTcStroeCommentList(tcStroeComment);
    }

    /**
     * 新增【请填写功能名称】
     *
     * @param tcStroeComment 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertTcStroeComment(TcStroeComment tcStroeComment) {
        tcStroeComment.setCreateTime(DateUtils.getNowDate());
        return tcStroeCommentMapper.insertTcStroeComment(tcStroeComment);
    }

    /**
     * 修改【请填写功能名称】
     *
     * @param tcStroeComment 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateTcStroeComment(TcStroeComment tcStroeComment) {
        return tcStroeCommentMapper.updateTcStroeComment(tcStroeComment);
    }

    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteTcStroeCommentByIds(Long[] ids) {
        return tcStroeCommentMapper.deleteTcStroeCommentByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     *
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteTcStroeCommentById(Long id) {
        return tcStroeCommentMapper.deleteTcStroeCommentById(id);
    }
}
