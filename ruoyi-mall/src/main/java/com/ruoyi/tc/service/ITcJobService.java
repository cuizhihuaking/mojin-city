package com.ruoyi.tc.service;

import com.ruoyi.tc.domain.TcJob;

import java.util.List;

/**
 * 招聘信息Service接口
 *
 * @author é­éåå
 * @date 2022-01-21
 */
public interface ITcJobService {
    /**
     * 查询招聘信息
     *
     * @param id 招聘信息ID
     * @return 招聘信息
     */
    public TcJob selectTcJobById(Long id);

    /**
     * 查询招聘信息列表
     *
     * @param tcJob 招聘信息
     * @return 招聘信息集合
     */
    public List<TcJob> selectTcJobList(TcJob tcJob);

    /**
     * 新增招聘信息
     *
     * @param tcJob 招聘信息
     * @return 结果
     */
    public int insertTcJob(TcJob tcJob);

    /**
     * 修改招聘信息
     *
     * @param tcJob 招聘信息
     * @return 结果
     */
    public int updateTcJob(TcJob tcJob);

    /**
     * 批量删除招聘信息
     *
     * @param ids 需要删除的招聘信息ID
     * @return 结果
     */
    public int deleteTcJobByIds(Long[] ids);

    /**
     * 删除招聘信息信息
     *
     * @param id 招聘信息ID
     * @return 结果
     */
    public int deleteTcJobById(Long id);

    /**
     * 当月发布数
     * @param userId
     * @return
     */
    Long countByMonth(Long userId);
}
