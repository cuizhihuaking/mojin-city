package com.ruoyi.tc.service;

import com.ruoyi.tc.domain.TcAd;

import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author é­éåå
 * @date 2022-01-21
 */
public interface ITcAdService {
    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public TcAd selectTcAdById(Long id);

    /**
     * 查询【请填写功能名称】列表
     *
     * @param tcAd 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<TcAd> selectTcAdList(TcAd tcAd);

    /**
     * 新增【请填写功能名称】
     *
     * @param tcAd 【请填写功能名称】
     * @return 结果
     */
    public int insertTcAd(TcAd tcAd);

    /**
     * 修改【请填写功能名称】
     *
     * @param tcAd 【请填写功能名称】
     * @return 结果
     */
    public int updateTcAd(TcAd tcAd);

    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的【请填写功能名称】ID
     * @return 结果
     */
    public int deleteTcAdByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     *
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteTcAdById(Long id);
}
