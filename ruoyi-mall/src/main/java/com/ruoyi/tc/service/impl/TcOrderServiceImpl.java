package com.ruoyi.tc.service.impl;

import java.util.List;
                                                                                import com.ruoyi.common.utils.DateUtils;
            import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ruoyi.tc.mapper.TcOrderMapper;
import com.ruoyi.tc.domain.TcOrder;
import com.ruoyi.tc.service.ITcOrderService;

/**
 * 订单Service业务层处理
 *
 * @author é­éåå
 * @date 2022-01-23
 */
@Slf4j
@Service
public class TcOrderServiceImpl implements ITcOrderService {

    @Autowired
    private TcOrderMapper tcOrderMapper;

    /**
     * 查询订单
     *
     * @param id 订单ID
     * @return 订单
     */
    @Override
    public TcOrder selectTcOrderById(Long id) {
        return tcOrderMapper.selectTcOrderById(id);
    }

    /**
     * 查询订单列表
     *
     * @param tcOrder 订单
     * @return 订单
     */
    @Override
    public List<TcOrder> selectTcOrderList(TcOrder tcOrder) {
        return tcOrderMapper.selectTcOrderList(tcOrder);
    }

    /**
     * 新增订单
     *
     * @param tcOrder 订单
     * @return 结果
     */
    @Override
    public int insertTcOrder(TcOrder tcOrder) {
                                                                                                                                        tcOrder.setCreateTime(DateUtils.getNowDate());
                                                                                                                                                                                            return tcOrderMapper.insertTcOrder(tcOrder);
    }

    /**
     * 修改订单
     *
     * @param tcOrder 订单
     * @return 结果
     */
    @Override
    public int updateTcOrder(TcOrder tcOrder) {
                                                                                                                                                                                                                                                                                                        return tcOrderMapper.updateTcOrder(tcOrder);
    }

    /**
     * 批量删除订单
     *
     * @param ids 需要删除的订单ID
     * @return 结果
     */
    @Override
    public int deleteTcOrderByIds(Long[] ids) {
        return tcOrderMapper.deleteTcOrderByIds(ids);
    }

    /**
     * 删除订单信息
     *
     * @param id 订单ID
     * @return 结果
     */
    @Override
    public int deleteTcOrderById(Long id) {
        return tcOrderMapper.deleteTcOrderById(id);
    }
}
