package com.ruoyi.tc.service;

import java.util.List;


import com.ruoyi.tc.domain.TcCollect;
import com.ruoyi.util.PageHelper;

/**
 * 商品关注Service接口
 *
 * @author é­éåå
 * @date 2022-01-23
 */
public interface ITcCollectService {
    /**
     * 查询商品关注
     *
     * @param id 商品关注ID
     * @return 商品关注
     */
    public TcCollect selectTcCollectById(Long id);

    /**
     * 查询商品关注列表
     *
     * @param tcCollect 商品关注
     * @return 商品关注集合
     */
    public List<TcCollect> selectTcCollectList(TcCollect tcCollect);

    /**
     * 新增商品关注
     *
     * @param tcCollect 商品关注
     * @return 结果
     */
    public int insertTcCollect(TcCollect tcCollect);

    /**
     * 修改商品关注
     *
     * @param tcCollect 商品关注
     * @return 结果
     */
    public int updateTcCollect(TcCollect tcCollect);

    /**
     * 批量删除商品关注
     *
     * @param ids 需要删除的商品关注ID
     * @return 结果
     */
    public int deleteTcCollectByIds(Long[] ids);

    /**
     * 删除商品关注信息
     *
     * @param id 商品关注ID
     * @return 结果
     */
    public int deleteTcCollectById(Long id);

    PageHelper<TcCollect> queryAttentionsForCustomerCentre(PageHelper<TcCollect> pageHelper, long customerId,Integer type);
}
