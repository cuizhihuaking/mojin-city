package com.ruoyi.tc.service.impl;

import java.util.List;
                                                                                import com.ruoyi.common.utils.DateUtils;
            import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ruoyi.tc.mapper.TcDepositRecordMapper;
import com.ruoyi.tc.domain.TcDepositRecord;
import com.ruoyi.tc.service.ITcDepositRecordService;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author é­éåå
 * @date 2022-01-23
 */
@Slf4j
@Service
public class TcDepositRecordServiceImpl implements ITcDepositRecordService {

    @Autowired
    private TcDepositRecordMapper tcDepositRecordMapper;

    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public TcDepositRecord selectTcDepositRecordById(Long id) {
        return tcDepositRecordMapper.selectTcDepositRecordById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     *
     * @param tcDepositRecord 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<TcDepositRecord> selectTcDepositRecordList(TcDepositRecord tcDepositRecord) {
        return tcDepositRecordMapper.selectTcDepositRecordList(tcDepositRecord);
    }

    /**
     * 新增【请填写功能名称】
     *
     * @param tcDepositRecord 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertTcDepositRecord(TcDepositRecord tcDepositRecord) {
                                                                                                                                        tcDepositRecord.setCreateTime(DateUtils.getNowDate());
                                                                                        return tcDepositRecordMapper.insertTcDepositRecord(tcDepositRecord);
    }

    /**
     * 修改【请填写功能名称】
     *
     * @param tcDepositRecord 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateTcDepositRecord(TcDepositRecord tcDepositRecord) {
                                                                                                                                                                                                    return tcDepositRecordMapper.updateTcDepositRecord(tcDepositRecord);
    }

    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteTcDepositRecordByIds(Long[] ids) {
        return tcDepositRecordMapper.deleteTcDepositRecordByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     *
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteTcDepositRecordById(Long id) {
        return tcDepositRecordMapper.deleteTcDepositRecordById(id);
    }
}
