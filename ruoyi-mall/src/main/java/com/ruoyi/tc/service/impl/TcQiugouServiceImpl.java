package com.ruoyi.tc.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.tc.domain.TcQiugou;
import com.ruoyi.tc.mapper.TcQiugouMapper;
import com.ruoyi.tc.service.ITcQiugouService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 求购Service业务层处理
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Slf4j
@Service
public class TcQiugouServiceImpl implements ITcQiugouService {

    @Autowired
    private TcQiugouMapper tcQiugouMapper;

    /**
     * 查询求购
     *
     * @param id 求购ID
     * @return 求购
     */
    @Override
    public TcQiugou selectTcQiugouById(Long id) {
        return tcQiugouMapper.selectTcQiugouById(id);
    }

    /**
     * 查询求购列表
     *
     * @param tcQiugou 求购
     * @return 求购
     */
    @Override
    public List<TcQiugou> selectTcQiugouList(TcQiugou tcQiugou) {
        return tcQiugouMapper.selectTcQiugouList(tcQiugou);
    }

    /**
     * 新增求购
     *
     * @param tcQiugou 求购
     * @return 结果
     */
    @Override
    public int insertTcQiugou(TcQiugou tcQiugou) {
        tcQiugou.setCreateTime(DateUtils.getNowDate());
        return tcQiugouMapper.insertTcQiugou(tcQiugou);
    }

    /**
     * 修改求购
     *
     * @param tcQiugou 求购
     * @return 结果
     */
    @Override
    public int updateTcQiugou(TcQiugou tcQiugou) {
        return tcQiugouMapper.updateTcQiugou(tcQiugou);
    }

    /**
     * 批量删除求购
     *
     * @param ids 需要删除的求购ID
     * @return 结果
     */
    @Override
    public int deleteTcQiugouByIds(Long[] ids) {
        return tcQiugouMapper.deleteTcQiugouByIds(ids);
    }

    /**
     * 删除求购信息
     *
     * @param id 求购ID
     * @return 结果
     */
    @Override
    public int deleteTcQiugouById(Long id) {
        return tcQiugouMapper.deleteTcQiugouById(id);
    }
    @Override
    public  Long countByMonth(Long customerId){
        return tcQiugouMapper.countByMonth(customerId);
    }
}
