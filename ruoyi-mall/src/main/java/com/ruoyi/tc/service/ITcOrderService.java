package com.ruoyi.tc.service;

import java.util.List;

import com.ruoyi.tc.domain.TcOrder;

/**
 * 订单Service接口
 *
 * @author é­éåå
 * @date 2022-01-23
 */
public interface ITcOrderService {
    /**
     * 查询订单
     *
     * @param id 订单ID
     * @return 订单
     */
    public TcOrder selectTcOrderById(Long id);

    /**
     * 查询订单列表
     *
     * @param tcOrder 订单
     * @return 订单集合
     */
    public List<TcOrder> selectTcOrderList(TcOrder tcOrder);

    /**
     * 新增订单
     *
     * @param tcOrder 订单
     * @return 结果
     */
    public int insertTcOrder(TcOrder tcOrder);

    /**
     * 修改订单
     *
     * @param tcOrder 订单
     * @return 结果
     */
    public int updateTcOrder(TcOrder tcOrder);

    /**
     * 批量删除订单
     *
     * @param ids 需要删除的订单ID
     * @return 结果
     */
    public int deleteTcOrderByIds(Long[] ids);

    /**
     * 删除订单信息
     *
     * @param id 订单ID
     * @return 结果
     */
    public int deleteTcOrderById(Long id);
}
