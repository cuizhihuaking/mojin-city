package com.ruoyi.tc.service;

import java.util.List;

import com.ruoyi.tc.domain.TcDepositRecord;

/**
 * 【请填写功能名称】Service接口
 *
 * @author é­éåå
 * @date 2022-01-23
 */
public interface ITcDepositRecordService {
    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public TcDepositRecord selectTcDepositRecordById(Long id);

    /**
     * 查询【请填写功能名称】列表
     *
     * @param tcDepositRecord 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<TcDepositRecord> selectTcDepositRecordList(TcDepositRecord tcDepositRecord);

    /**
     * 新增【请填写功能名称】
     *
     * @param tcDepositRecord 【请填写功能名称】
     * @return 结果
     */
    public int insertTcDepositRecord(TcDepositRecord tcDepositRecord);

    /**
     * 修改【请填写功能名称】
     *
     * @param tcDepositRecord 【请填写功能名称】
     * @return 结果
     */
    public int updateTcDepositRecord(TcDepositRecord tcDepositRecord);

    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的【请填写功能名称】ID
     * @return 结果
     */
    public int deleteTcDepositRecordByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     *
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteTcDepositRecordById(Long id);
}
