package com.ruoyi.tc.service;

import com.ruoyi.tc.domain.TcXieyi;

import java.util.List;

/**
 * 标签Service接口
 *
 * @author é­éåå
 * @date 2022-01-21
 */
public interface ITcXieyiService {
    /**
     * 查询标签
     *
     * @param id 标签ID
     * @return 标签
     */
    public TcXieyi selectTcXieyiById(Long id);

    /**
     * 查询标签列表
     *
     * @param tcXieyi 标签
     * @return 标签集合
     */
    public List<TcXieyi> selectTcXieyiList(TcXieyi tcXieyi);

    /**
     * 新增标签
     *
     * @param tcXieyi 标签
     * @return 结果
     */
    public int insertTcXieyi(TcXieyi tcXieyi);

    /**
     * 修改标签
     *
     * @param tcXieyi 标签
     * @return 结果
     */
    public int updateTcXieyi(TcXieyi tcXieyi);

    /**
     * 批量删除标签
     *
     * @param ids 需要删除的标签ID
     * @return 结果
     */
    public int deleteTcXieyiByIds(Long[] ids);

    /**
     * 删除标签信息
     *
     * @param id 标签ID
     * @return 结果
     */
    public int deleteTcXieyiById(Long id);
}
