package com.ruoyi.tc.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.tc.domain.TcUserCard;
import com.ruoyi.tc.mapper.TcUserCardMapper;
import com.ruoyi.tc.service.ITcUserCardService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 人才简历Service业务层处理
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Slf4j
@Service
public class TcUserCardServiceImpl implements ITcUserCardService {

    @Autowired
    private TcUserCardMapper tcUserCardMapper;

    /**
     * 查询人才简历
     *
     * @param id 人才简历ID
     * @return 人才简历
     */
    @Override
    public TcUserCard selectTcUserCardById(Long id) {
        return tcUserCardMapper.selectTcUserCardById(id);
    }

    /**
     * 查询人才简历列表
     *
     * @param tcUserCard 人才简历
     * @return 人才简历
     */
    @Override
    public List<TcUserCard> selectTcUserCardList(TcUserCard tcUserCard) {
        return tcUserCardMapper.selectTcUserCardList(tcUserCard);
    }

    /**
     * 新增人才简历
     *
     * @param tcUserCard 人才简历
     * @return 结果
     */
    @Override
    public int insertTcUserCard(TcUserCard tcUserCard) {
        tcUserCard.setCreateTime(DateUtils.getNowDate());
        return tcUserCardMapper.insertTcUserCard(tcUserCard);
    }

    /**
     * 修改人才简历
     *
     * @param tcUserCard 人才简历
     * @return 结果
     */
    @Override
    public int updateTcUserCard(TcUserCard tcUserCard) {
        return tcUserCardMapper.updateTcUserCard(tcUserCard);
    }

    /**
     * 批量删除人才简历
     *
     * @param ids 需要删除的人才简历ID
     * @return 结果
     */
    @Override
    public int deleteTcUserCardByIds(Long[] ids) {
        return tcUserCardMapper.deleteTcUserCardByIds(ids);
    }

    /**
     * 删除人才简历信息
     *
     * @param id 人才简历ID
     * @return 结果
     */
    @Override
    public int deleteTcUserCardById(Long id) {
        return tcUserCardMapper.deleteTcUserCardById(id);
    }
    @Override
    public  Long countByMonth(Long customerId){
        return tcUserCardMapper.countByMonth(customerId);
    }
}
