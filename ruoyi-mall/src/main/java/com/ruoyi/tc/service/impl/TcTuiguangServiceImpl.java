package com.ruoyi.tc.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.tc.domain.TcTuiguang;
import com.ruoyi.tc.mapper.TcTuiguangMapper;
import com.ruoyi.tc.service.ITcTuiguangService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 推广Service业务层处理
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Slf4j
@Service
public class TcTuiguangServiceImpl implements ITcTuiguangService {

    @Autowired
    private TcTuiguangMapper tcTuiguangMapper;

    /**
     * 查询推广
     *
     * @param id 推广ID
     * @return 推广
     */
    @Override
    public TcTuiguang selectTcTuiguangById(Long id) {
        return tcTuiguangMapper.selectTcTuiguangById(id);
    }

    /**
     * 查询推广列表
     *
     * @param tcTuiguang 推广
     * @return 推广
     */
    @Override
    public List<TcTuiguang> selectTcTuiguangList(TcTuiguang tcTuiguang) {
        return tcTuiguangMapper.selectTcTuiguangList(tcTuiguang);
    }

    /**
     * 新增推广
     *
     * @param tcTuiguang 推广
     * @return 结果
     */
    @Override
    public int insertTcTuiguang(TcTuiguang tcTuiguang) {
        tcTuiguang.setCreateTime(DateUtils.getNowDate());
        return tcTuiguangMapper.insertTcTuiguang(tcTuiguang);
    }

    /**
     * 修改推广
     *
     * @param tcTuiguang 推广
     * @return 结果
     */
    @Override
    public int updateTcTuiguang(TcTuiguang tcTuiguang) {
        return tcTuiguangMapper.updateTcTuiguang(tcTuiguang);
    }

    /**
     * 批量删除推广
     *
     * @param ids 需要删除的推广ID
     * @return 结果
     */
    @Override
    public int deleteTcTuiguangByIds(Long[] ids) {
        return tcTuiguangMapper.deleteTcTuiguangByIds(ids);
    }

    /**
     * 删除推广信息
     *
     * @param id 推广ID
     * @return 结果
     */
    @Override
    public int deleteTcTuiguangById(Long id) {
        return tcTuiguangMapper.deleteTcTuiguangById(id);
    }
    @Override
    public  Long countByMonth(Long customerId){
        return tcTuiguangMapper.countByMonth(customerId);
    }
}
