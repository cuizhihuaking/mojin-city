package com.ruoyi.tc.vo;

import com.ruoyi.tc.domain.TcNav;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class TcTcNavVo extends TcNav {
    private List<TcNav> children =new ArrayList<>();
}
