package com.ruoyi.tc.vo;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TcPayVo {

    private String code;
    private Long id;
}
