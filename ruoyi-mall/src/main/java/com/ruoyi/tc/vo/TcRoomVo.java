package com.ruoyi.tc.vo;


import com.ruoyi.tc.domain.TcAd;
import com.ruoyi.tc.domain.TcNav;
import com.ruoyi.tc.domain.TcRoom;
import com.ruoyi.tc.domain.TcStoreInfo;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("首页数据")
public class TcRoomVo {


    List<TcAd> banner;

    List<TcRoom> rent_list;

    List<TcRoom> sell_list;

    List<TcRoom> shop_list;



}
