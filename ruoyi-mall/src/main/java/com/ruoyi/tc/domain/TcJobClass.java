package com.ruoyi.tc.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * 职位分类对象 tc_job_class
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Setter
@Getter
public class TcJobClass extends BaseEntity {
    private static final long serialVersionUID = 1L;

    public boolean isFirstCategory() {
        if (this.parentCode!=null){
            return this.parentCode == -1;
        }
        return false;
    }
    private List<TcJobClass> children =new ArrayList<>();
    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * $column.columnComment
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String name;

    /**
     * $column.columnComment
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long code;

    /**
     * $column.columnComment
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long parentCode;

    /**
     * $column.columnComment
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String allName;

    /**
     * $column.columnComment
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String allPinyinName;


}
