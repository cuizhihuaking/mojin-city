package com.ruoyi.tc.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * 模块对象 tc_nav
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Setter
@Getter
public class TcNav extends BaseEntity {
    private static final long serialVersionUID = 1L;

    public boolean isFirstCategory() {
        if (this.pid!=null){
            return this.pid == 0;
        }
       return false;
    }

    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * $column.columnComment
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String name;

    /**
     * 轮播位置：1首页 2 商家页
     */
    @Excel(name = "轮播位置：home 首页 find 商家页")
    private String type;

    /**
     * $column.columnComment
     */
    @Excel(name = "轮播位置：1首页 2 商家页")
    private String pic;

    /**
     * 上下线状态：0->下线；1->上线
     */
    @Excel(name = "上下线状态：hidden->下线；normal->上线")
    private String status;

    /**
     * 权重
     */
    @Excel(name = "权重")
    private Long weight;

    /**
     * 下单数
     */
    @Excel(name = "下单数")
    private Long pid;

    /**
     * 链接地址
     */
    @Excel(name = "链接地址")
    private String url;

    /**
     * 排序
     */
    @Excel(name = "排序")
    private Long sort;


}
