package com.ruoyi.tc.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 【请填写功能名称】对象 tc_ad
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Setter
@Getter
public class TcAd extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * 首页轮播	1
     * 首页单广告	2
     * 商家页轮播	3
     * 房源单广告	4
     * 顺风车轮播	5
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String type;

    /**
     * $column.columnComment
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String pic;

    /**
     * $column.columnComment
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String path;

    /**
     * $column.columnComment
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long hit;


}
