/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:TcMemberMapper.java
 * Date:2020/11/27 16:26:27
 */

package com.ruoyi.tc.mapper;


import com.ruoyi.tc.domain.TcMember;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 会员Mapper接口
 *
 * @author 魔金商城
 * @date 2020-07-25
 */
public interface TcMemberMapper {
    /**
     * 查询会员总数
     *
     * @param params 查询参数
     * @return 返回会员总数
     */

    int queryCustomerCount(Map<String, Object> params);

    /**
     * 查询会员数据
     *
     * @param params 查询参数
     * @return 返回会员数据
     */

    List<TcMember> queryCustomers(Map<String, Object> params);

    TcMember queryCustomerByAliOpenId(@Param("openId") String openid, @Param("storeId") long storeId);


    TcMember queryCustomerByh5OpenId(@Param("openId") String openid, @Param("storeId") long storeId);

    TcMember queryCustomerByappOpenId(@Param("openId") String openid, @Param("storeId") long storeId);

    TcMember queryCustomerByappletOpenId(@Param("openId") String openid, @Param("storeId") long storeId);

    /**
     * 根据会员id查询会员信息
     *
     * @param id 会员id
     * @return 返回会员信息
     */

    TcMember queryCustomerById(long id);

    /**
     * 删除会员信息
     *
     * @param ids 会员集合
     * @return 成功返回>1 失败返回0
     */

    int deleteCustomers(List<Long> ids);




    /**
     * 修改会员信息
     *
     * @param customer 会员信息
     * @return 成功返回 1 失败返回0
     */

    int updateCustomer(TcMember customer);

    /**
     * 更新最后登录时间
     *
     * @param customerId 会员id
     * @return 更新返回码
     */

    int updateLoginTime(long customerId);

    /**
     * 查询店铺员工总条数
     *
     * @param map 查询条件
     * @return 店铺员工总条数
     */

    int queryStoreStallCount(Map<String, Object> map);




    /**
     * 更新会员storeId和type
     *
     * @param customer 会员实体类
     * @return 添加返回码
     */

    int updateStoreIdAndType(TcMember customer);


    /**
     * 更新会员个人信息
     *
     * @param map 更新参数
     * @return 更新返回码
     */

    int updatePersonalInfo(Map<String, Object> map);

    /**
     * 根据用户名查询用户信息(用户名,手机,邮箱)
     *
     * @param params 查询参数
     * @return 返回用户信息
     */

    TcMember queryCustomerByName(Map<String, Object> params);


    /**
     * 解锁用户
     *
     * @param customerId 会员id
     * @return 成功返回1 失败返回0
     */

    int unlockUser(long customerId);

    /**
     * 增加登录错误次数
     *
     * @param customerId 会员id
     * @return 成功返回1 失败返回0
     */

    int updateLoginErrorCount(long customerId);

    /**
     * 锁定用户
     *
     * @param customerId 用户id
     * @return 成功返回1 失败返回0
     */

    int lockUser(long customerId);

    /**
     * 根据手机号码查询总数
     *
     * @param mobile 手机号码
     * @return 返回手机号码对应的用户总数
     */

    int queryByMobile(@Param("mobile") String mobile, @Param("storeId") long storeId);

    /**
     * 根据邮箱查询总数
     *
     * @param email 邮箱
     * @return 返回邮箱对应的用户总数
     */

    int queryByEmail(@Param("email") String email, @Param("storeId") long storeId);

    /**
     * 修改用户密码
     *
     * @param params 参数
     * @return 成功返回1 失败返回0
     */

    int updatePassword(Map<String, Object> params);

    /**
     * 修改用户支付密码
     *
     * @param params 参数
     * @return 成功返回1 失败返回0
     */

    int updatePayPassword(Map<String, Object> params);

    /**
     * 重新绑定手机号码
     *
     * @param params 参数
     * @return 成功返回1 失败返回0
     */

    int bindNewMobile(Map<String, Object> params);

    /**
     * 根据手机号码修改密码
     *
     * @param params 参数
     * @return 成功返回1 失败返回0
     */

    int updatePasswordByMobile(Map<String, Object> params);

    /**
     * 修改用户总的消费金额
     *
     * @param params 参数
     * @return 成功返回1 失败返回0
     */

    int updateCustomerConsumptionAmount(Map<String, Object> params);

    /**
     * 查找用户信息
     *
     * @param ids 用户id集合
     * @return 用户信息集合
     */

    List<TcMember> queryCustomersByIds(@Param("ids") List<Long> ids, @Param("storeId") long storeId);

    /**
     * 查找所有用户信息
     */

    List<TcMember> queryAllCustomer();

    /**
     * 更新需更改的邮箱信息
     *
     * @param params 参数
     * @return 成功返回1 失败返回0
     */

    int updateModifiedEmailInfo(Map<String, Object> params);

    /**
     * 更新邮箱信息
     *
     * @param customerId 用户id
     * @return 成功返回1 失败返回0
     */

    int modifiedEmail(long customerId);

    /**
     * 查找需更新的邮箱信息
     *
     * @param params 参数
     * @return 用户实体
     */

    TcMember selectModifiedEmailInfo(Map<String, Object> params);

    /**
     * 更新签到次数
     *
     * @param params 参数
     * @return 1:成功，否则失败
     */

    int updateSignNum(Map<String, Object> params);



    /**
     * 统计新增用户数量（按日期分组）总组数
     *
     * @param params 参数
     * @return 返回按日期分组的新增用户数量总组数
     */

    int queryNewCustomerStatisticsCount(Map<String, Object> params);




    /**
     * 更新用户佣金
     *
     * @param params 参数
     * @return 成功>0
     */

    int updateCustomerCommission(Map<String, Object> params);

    /**
     * 查询今日新增会员数
     *
     * @return 返回今日新增会员数
     */

    int queryNewCustomerToday();

    /**
     * 查询本周新增会员数
     *
     * @return 返回本周新增会员数
     */

    int queryNewCustomerThisWeek();




    /**
     * 查找所有没有开店的用户手机号
     *
     * @return 没有开店用户手机号集合
     */

    List<String> queryAllCustomerMobileForCreateStore();

    /**
     * 查询所有有分销下级的会员id集合
     *
     * @return 会员id集合
     */

    List<Long> querySpreadCustomerIdList(Map<String, Object> params);

    /**
     * 查询所有有分销下级的会员数量
     *
     * @return 会员数量
     */

    int querySpreadCustomerIdListCount(Map<String, Object> params);

    /**
     * 根据会员id查询分销下级会员
     *
     * @return 下级会员列表
     */

    List<TcMember> querySpreadCustomerByCustomerId(Map<String, Object> params);

    /**
     * 根据会员id查询分销下级会员数量
     *
     * @param customerId 会员id
     * @return 下级会员数量
     */

    int querySpreadCustomerCountByCustomerId(long customerId);

    /**
     * 查询会员
     *
     * @param id 会员ID
     * @return 会员
     */
    public TcMember selectTcMemberById(Long id);

    /**
     * 查询会员列表
     *
     * @param TcMember 会员
     * @return 会员集合
     */
    public List<TcMember> selectTcMemberList(TcMember TcMember);

    /**
     * 新增会员
     *
     * @param TcMember 会员
     * @return 结果
     */
    public int insertTcMember(TcMember TcMember);

    /**
     * 修改会员
     *
     * @param TcMember 会员
     * @return 结果
     */
    public int updateTcMember(TcMember TcMember);

    /**
     * 删除会员
     *
     * @param id 会员ID
     * @return 结果
     */
    public int deleteTcMemberById(Long id);

    /**
     * 批量删除会员
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTcMemberByIds(Long[] ids);

    TcMember queryCustomerByRecommondCode(@Param("code") String code, @Param("storeId") long storeId);
}
