package com.ruoyi.tc.mapper;

import com.ruoyi.tc.domain.TcNav;

import java.util.List;

/**
 * 模块Mapper接口
 *
 * @author é­éåå
 * @date 2022-01-21
 */
public interface TcNavMapper {
    /**
     * 查询模块
     *
     * @param id 模块ID
     * @return 模块
     */
    public TcNav selectTcNavById(Long id);


    /**
     * 批量查询模块
     *
     * @param ids 需要查询的数据ID
     * @return 结果
     */
    public List<TcNav> selectTcNavByIds(Long[] ids);

    /**
     * 查询模块列表
     *
     * @param tcNav 模块
     * @return 模块集合
     */
    public List<TcNav> selectTcNavList(TcNav tcNav);

    /**
     * 新增模块
     *
     * @param tcNav 模块
     * @return 结果
     */
    public int insertTcNav(TcNav tcNav);

    /**
     * 修改模块
     *
     * @param tcNav 模块
     * @return 结果
     */
    public int updateTcNav(TcNav tcNav);

    /**
     * 删除模块
     *
     * @param id 模块ID
     * @return 结果
     */
    public int deleteTcNavById(Long id);

    /**
     * 批量删除模块
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTcNavByIds(Long[] ids);
}
