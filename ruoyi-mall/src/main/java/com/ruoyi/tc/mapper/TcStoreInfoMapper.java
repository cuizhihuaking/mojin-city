package com.ruoyi.tc.mapper;

import com.ruoyi.tc.domain.TcStoreInfo;

import java.util.List;

/**
 * 商家店铺Mapper接口
 *
 * @author é­éåå
 * @date 2022-01-21
 */
public interface TcStoreInfoMapper {
    /**
     * 查询商家店铺
     *
     * @param id 商家店铺ID
     * @return 商家店铺
     */
    public TcStoreInfo selectTcStoreInfoById(Long id);


    /**
     * 批量查询商家店铺
     *
     * @param ids 需要查询的数据ID
     * @return 结果
     */
    public List<TcStoreInfo> selectTcStoreInfoByIds(Long[] ids);

    /**
     * 查询商家店铺列表
     *
     * @param tcStoreInfo 商家店铺
     * @return 商家店铺集合
     */
    public List<TcStoreInfo> selectTcStoreInfoList(TcStoreInfo tcStoreInfo);

    public List<TcStoreInfo> selectTcStoreInfoListByDistinct(TcStoreInfo tcStoreInfo);


    /**
     * 新增商家店铺
     *
     * @param tcStoreInfo 商家店铺
     * @return 结果
     */
    public int insertTcStoreInfo(TcStoreInfo tcStoreInfo);

    /**
     * 修改商家店铺
     *
     * @param tcStoreInfo 商家店铺
     * @return 结果
     */
    public int updateTcStoreInfo(TcStoreInfo tcStoreInfo);

    /**
     * 删除商家店铺
     *
     * @param id 商家店铺ID
     * @return 结果
     */
    public int deleteTcStoreInfoById(Long id);

    /**
     * 批量删除商家店铺
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTcStoreInfoByIds(Long[] ids);
}
