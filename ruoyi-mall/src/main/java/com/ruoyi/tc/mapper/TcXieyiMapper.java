package com.ruoyi.tc.mapper;

import com.ruoyi.tc.domain.TcXieyi;

import java.util.List;

/**
 * 标签Mapper接口
 *
 * @author é­éåå
 * @date 2022-01-21
 */
public interface TcXieyiMapper {
    /**
     * 查询标签
     *
     * @param id 标签ID
     * @return 标签
     */
    public TcXieyi selectTcXieyiById(Long id);


    /**
     * 批量查询标签
     *
     * @param ids 需要查询的数据ID
     * @return 结果
     */
    public List<TcXieyi> selectTcXieyiByIds(Long[] ids);

    /**
     * 查询标签列表
     *
     * @param tcXieyi 标签
     * @return 标签集合
     */
    public List<TcXieyi> selectTcXieyiList(TcXieyi tcXieyi);

    /**
     * 新增标签
     *
     * @param tcXieyi 标签
     * @return 结果
     */
    public int insertTcXieyi(TcXieyi tcXieyi);

    /**
     * 修改标签
     *
     * @param tcXieyi 标签
     * @return 结果
     */
    public int updateTcXieyi(TcXieyi tcXieyi);

    /**
     * 删除标签
     *
     * @param id 标签ID
     * @return 结果
     */
    public int deleteTcXieyiById(Long id);

    /**
     * 批量删除标签
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTcXieyiByIds(Long[] ids);
}
