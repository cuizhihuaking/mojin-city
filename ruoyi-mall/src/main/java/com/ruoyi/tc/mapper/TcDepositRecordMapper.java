package com.ruoyi.tc.mapper;

import java.util.List;

import com.ruoyi.tc.domain.TcDepositRecord;

/**
 * 【请填写功能名称】Mapper接口
 *
 * @author é­éåå
 * @date 2022-01-23
 */
public interface TcDepositRecordMapper {
    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public TcDepositRecord selectTcDepositRecordById(Long id);


      /**
         * 批量查询【请填写功能名称】
         *
         * @param ids 需要查询的数据ID
         * @return 结果
         */
      public List<TcDepositRecord>  selectTcDepositRecordByIds(Long[] ids);

    /**
     * 查询【请填写功能名称】列表
     *
     * @param tcDepositRecord 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<TcDepositRecord> selectTcDepositRecordList(TcDepositRecord tcDepositRecord);

    /**
     * 新增【请填写功能名称】
     *
     * @param tcDepositRecord 【请填写功能名称】
     * @return 结果
     */
    public int insertTcDepositRecord(TcDepositRecord tcDepositRecord);

    /**
     * 修改【请填写功能名称】
     *
     * @param tcDepositRecord 【请填写功能名称】
     * @return 结果
     */
    public int updateTcDepositRecord(TcDepositRecord tcDepositRecord);

    /**
     * 删除【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteTcDepositRecordById(Long id);

    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTcDepositRecordByIds(Long[] ids);
}
