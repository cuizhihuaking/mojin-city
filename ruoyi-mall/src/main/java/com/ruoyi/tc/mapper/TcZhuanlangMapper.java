package com.ruoyi.tc.mapper;

import com.ruoyi.tc.domain.TcZhuanlang;

import java.util.List;

/**
 * 转让Mapper接口
 *
 * @author é­éåå
 * @date 2022-01-21
 */
public interface TcZhuanlangMapper {
    /**
     * 查询转让
     *
     * @param id 转让ID
     * @return 转让
     */
    public TcZhuanlang selectTcZhuanlangById(Long id);


    /**
     * 批量查询转让
     *
     * @param ids 需要查询的数据ID
     * @return 结果
     */
    public List<TcZhuanlang> selectTcZhuanlangByIds(Long[] ids);

    /**
     * 查询转让列表
     *
     * @param tcZhuanlang 转让
     * @return 转让集合
     */
    public List<TcZhuanlang> selectTcZhuanlangList(TcZhuanlang tcZhuanlang);

    /**
     * 新增转让
     *
     * @param tcZhuanlang 转让
     * @return 结果
     */
    public int insertTcZhuanlang(TcZhuanlang tcZhuanlang);

    /**
     * 修改转让
     *
     * @param tcZhuanlang 转让
     * @return 结果
     */
    public int updateTcZhuanlang(TcZhuanlang tcZhuanlang);

    /**
     * 删除转让
     *
     * @param id 转让ID
     * @return 结果
     */
    public int deleteTcZhuanlangById(Long id);

    /**
     * 批量删除转让
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTcZhuanlangByIds(Long[] ids);

    Long countByMonth(long customerId);
}
