/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:LoginParams.java
 * Date:2020/11/27 16:26:27
 */

package com.ruoyi.member.vo;


import com.ruoyi.tc.domain.TcMember;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.function.Consumer;


/**
 * 登入参数实体
 */
@Data
@ApiModel(description = "登入参数实体")
public class LoginParams {

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    private String mobile;

    private long storeId = 0;
    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String password;

    /**
     * 回调函数
     */
    @ApiModelProperty(value = "回调函数")
    private Consumer<TcMember> consumer;

    /**
     * 用户输入的验证码
     */
    @ApiModelProperty(value = "用户输入的验证码")
    private String code;

    /**
     * session中的验证码
     */
    @ApiModelProperty(value = "session中的验证码")
    private String codeInSession;

    /**
     * 推荐人code 如果为空 则说明没有推荐人
     */
    private String recommondCode;
    /**
     * 登录来源 0 pc  1 app 2 mobile
     */
    private int source;

    public void setFromApp() {
        this.source = 1;
    }

    /**
     * 验证验证码 是否正确 （目前都不需要验证码）
     *
     * @return 正确返回true  否则返回false
     */
    public boolean validateCode() {

        // 目前都不需要验证码
        return true;
    }

}
