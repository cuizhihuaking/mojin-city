/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:AppletLoginRedisParamResponse.java
 * Date:2020/11/27 16:26:27
 */

package com.ruoyi.member.vo;

import lombok.Data;
import org.springframework.util.StringUtils;

/**
 * 小程序redis参数返回实体类
 *
 * @author SK
 * @since 2018/6/13
 */
@Data
public class AppletLoginRedisParamResponse {

    private UserInfo userInfo;
    /**
     * 小程序sessionKey
     */
    private String sessionKey;
    /**
     * token
     */
    private String token;
    /**
     * 联合登录id
     */
    private String unionId;
    /**
     * 用户id
     */
    private long customerId = -1;
    private long storeId = 0;
    /**
     * 推荐人code 如果为空 则说明没有推荐人
     */
    private String recommondCode;
    /**
     * 微信用户标识
     */
    private String openId;

    private String code;
    private String encryptedData;

    private String iv;
    /**
     * 用户注册来源 1 pc  2app  3 手机h5 4 管理员后台新增 5 微信小程序 6支付宝小程序 7 头条小程序 8 百度小程序 9 qq小程序
     */
    private String source;

    /**
     * 判断是否有unionId
     *
     * @return 有返回true, 否则返回false
     */
    public boolean hasUnionId() {
        return !StringUtils.isEmpty(unionId);
    }
}
