/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:SysFile.java
 * Date:2020/09/13 08:43:13
 */

package com.ruoyi.security.web.domain.server;

import lombok.Getter;
import lombok.Setter;

/**
 * 系统文件相关信息
 *
 * @author ruoyi
 */
@Setter
@Getter
public class SysFile {
    /**
     * 盘符路径
     */
    private String dirName;

    /**
     * 盘符类型
     */
    private String sysTypeName;

    /**
     * 文件类型
     */
    private String typeName;

    /**
     * 总大小
     */
    private String total;

    /**
     * 剩余大小
     */
    private String free;

    /**
     * 已经使用量
     */
    private String used;

    /**
     * 资源的使用率
     */
    private double usage;

    /**
     * 文件名
     */
    private String fileName;


    /**
     * 文件前缀
     */
    private String imgUrlPrefix;


}
