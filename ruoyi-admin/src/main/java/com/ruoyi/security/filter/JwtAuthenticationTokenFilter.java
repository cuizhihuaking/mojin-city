/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:JwtAuthenticationTokenFilter.java
 * Date:2021/01/09 11:40:09
 */

package com.ruoyi.security.filter;

import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.security.LoginUser;
import com.ruoyi.security.SecurityUtils;
import com.ruoyi.security.TokenService;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

/**
 * token过滤器 验证token有效性
 *
 * @author ruoyi
 */
@Component
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {
    @Autowired
    private TokenService tokenService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {
        logger.debug(request.getRequestURI() + "," + request.getMethod());
        String requestIdKey = "requestId";
        String requestId = UUID.randomUUID().toString().replace("-", "");
        MDC.put(requestIdKey, requestId);
        LoginUser loginUser = tokenService.getLoginUser(request);
        if (StringUtils.isNotNull(loginUser) && StringUtils.isNull(SecurityUtils.getAuthentication())) {
            tokenService.verifyToken(loginUser);
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loginUser, null, loginUser.getAuthorities());
            authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        }
        try {
            chain.doFilter(request, response);
        } finally {
            MDC.remove(requestIdKey);
        }

    }
}
