package com.ruoyi.web.controller.tc;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.tc.domain.TcDepositRecord;
import com.ruoyi.tc.service.ITcDepositRecordService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 *
 * @author é­éåå
 * @date 2022-01-23
 */
@RestController
@RequestMapping("/tc/TcDepositRecord")
public class TcDepositRecordController extends BaseController {
    @Autowired
    private ITcDepositRecordService tcDepositRecordService;

/**
 * 查询【请填写功能名称】列表
 */
@PreAuthorize("@ss.hasPermi('tc:TcDepositRecord:list')")
@GetMapping("/list")
        public TableDataInfo list(TcDepositRecord tcDepositRecord) {
        startPage();
        List<TcDepositRecord> list = tcDepositRecordService.selectTcDepositRecordList(tcDepositRecord);
        return getDataTable(list);
    }
    
    /**
     * 导出【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcDepositRecord:export')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TcDepositRecord tcDepositRecord) {
        List<TcDepositRecord> list = tcDepositRecordService.selectTcDepositRecordList(tcDepositRecord);
        ExcelUtil<TcDepositRecord> util = new ExcelUtil<TcDepositRecord>(TcDepositRecord. class);
        return util.exportExcel(list, "TcDepositRecord");
    }

    /**
     * 获取【请填写功能名称】详细信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcDepositRecord:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tcDepositRecordService.selectTcDepositRecordById(id));
    }

    /**
     * 新增【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('tc:TcDepositRecord:add')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TcDepositRecord tcDepositRecord) {
        return toAjax(tcDepositRecordService.insertTcDepositRecord(tcDepositRecord));
    }

    /**
     * 修改【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('tc:TcDepositRecord:edit')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TcDepositRecord tcDepositRecord) {
        return toAjax(tcDepositRecordService.updateTcDepositRecord(tcDepositRecord));
    }

    /**
     * 删除【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('tc:TcDepositRecord:remove')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tcDepositRecordService.deleteTcDepositRecordByIds(ids));
    }
}
