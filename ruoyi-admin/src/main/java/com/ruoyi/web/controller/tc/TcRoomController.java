package com.ruoyi.web.controller.tc;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ConvertUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.tc.domain.TcGoods;
import com.ruoyi.tc.domain.TcRoom;
import com.ruoyi.tc.service.ITcRoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 房产信息Controller
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@RestController
@RequestMapping("/tc/TcRoom")
public class TcRoomController extends BaseController {
    @Autowired
    private ITcRoomService tcRoomService;

    /**
     * 查询房产信息列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcRoom:list')")
    @GetMapping("/list")
    public TableDataInfo list(TcRoom tcRoom) {
        startPage();
        List<TcRoom> list = tcRoomService.selectTcRoomList(tcRoom);
        return getDataTable(list);
    }

    /**
     * 导出房产信息列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcRoom:export')")
    @Log(title = "房产信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TcRoom tcRoom) {
        List<TcRoom> list = tcRoomService.selectTcRoomList(tcRoom);
        ExcelUtil<TcRoom> util = new ExcelUtil<TcRoom>(TcRoom.class);
        return util.exportExcel(list, "TcRoom");
    }

    /**
     * 获取房产信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcRoom:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        TcRoom tcGoods =tcRoomService.selectTcRoomById(id);
        if (StringUtils.isNotEmpty(tcGoods.getTagIds())){
            tcGoods.setTagIdsIds(tcGoods.getTagIds().split(","));
        }
        if (StringUtils.isNotEmpty(tcGoods.getConfigIds())){
            tcGoods.setConfigIdsIds(tcGoods.getConfigIds().split(","));
        }
        return AjaxResult.success(tcGoods);
    }

    /**
     * 新增房产信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcRoom:add')")
    @Log(title = "房产信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TcRoom tcRoom) {
        return toAjax(tcRoomService.insertTcRoom(tcRoom));
    }

    /**
     * 修改房产信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcRoom:edit')")
    @Log(title = "房产信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TcRoom tcRoom) {
        return toAjax(tcRoomService.updateTcRoom(tcRoom));
    }

    /**
     * 删除房产信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcRoom:remove')")
    @Log(title = "房产信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tcRoomService.deleteTcRoomByIds(ids));
    }
}
