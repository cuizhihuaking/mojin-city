package com.ruoyi.web.controller.tc;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysRole;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ConvertUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.tc.domain.TcJob;
import com.ruoyi.tc.service.ITcJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 招聘信息Controller
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@RestController
@RequestMapping("/tc/TcJob")
public class TcJobController extends BaseController {
    @Autowired
    private ITcJobService tcJobService;

    /**
     * 查询招聘信息列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcJob:list')")
    @GetMapping("/list")
    public TableDataInfo list(TcJob tcJob) {
        startPage();
        List<TcJob> list = tcJobService.selectTcJobList(tcJob);
        return getDataTable(list);
    }

    /**
     * 导出招聘信息列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcJob:export')")
    @Log(title = "招聘信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TcJob tcJob) {
        List<TcJob> list = tcJobService.selectTcJobList(tcJob);
        ExcelUtil<TcJob> util = new ExcelUtil<TcJob>(TcJob.class);
        return util.exportExcel(list, "TcJob");
    }

    /**
     * 获取招聘信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcJob:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        AjaxResult ajax = AjaxResult.success();
        if (StringUtils.isNotNull(id)) {
            TcJob tcJob =tcJobService.selectTcJobById(id);
            ajax.put(AjaxResult.DATA_TAG, tcJob);
            if (StringUtils.isNotEmpty(tcJob.getSalaryLevel())){
                ajax.put("salaryLevelIds", tcJob.getSalaryLevel().split(","));
            }

        }
        return ajax;
    }

    /**
     * 新增招聘信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcJob:add')")
    @Log(title = "招聘信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TcJob tcJob) {
        return toAjax(tcJobService.insertTcJob(tcJob));
    }

    /**
     * 修改招聘信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcJob:edit')")
    @Log(title = "招聘信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TcJob tcJob) {

        return toAjax(tcJobService.updateTcJob(tcJob));
    }

    /**
     * 删除招聘信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcJob:remove')")
    @Log(title = "招聘信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tcJobService.deleteTcJobByIds(ids));
    }
}
