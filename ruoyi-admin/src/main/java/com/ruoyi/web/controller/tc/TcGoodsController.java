package com.ruoyi.web.controller.tc;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ConvertUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.tc.domain.TcGoods;
import com.ruoyi.tc.service.ITcGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 物品信息Controller
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@RestController
@RequestMapping("/tc/TcGoods")
public class TcGoodsController extends BaseController {
    @Autowired
    private ITcGoodsService tcGoodsService;

    /**
     * 查询物品信息列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcGoods:list')")
    @GetMapping("/list")
    public TableDataInfo list(TcGoods tcGoods) {
        startPage();
        List<TcGoods> list = tcGoodsService.selectTcGoodsList(tcGoods);
        return getDataTable(list);
    }

    /**
     * 导出物品信息列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcGoods:export')")
    @Log(title = "物品信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TcGoods tcGoods) {
        List<TcGoods> list = tcGoodsService.selectTcGoodsList(tcGoods);
        ExcelUtil<TcGoods> util = new ExcelUtil<TcGoods>(TcGoods.class);
        return util.exportExcel(list, "TcGoods");
    }

    /**
     * 获取物品信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcGoods:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        TcGoods tcGoods =tcGoodsService.selectTcGoodsById(id);
        if (StringUtils.isNotEmpty(tcGoods.getTagIds())){
            tcGoods.setTagIdsIds(tcGoods.getTagIds().split(","));
        }
        return AjaxResult.success(tcGoods);
    }

    /**
     * 新增物品信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcGoods:add')")
    @Log(title = "物品信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TcGoods tcGoods) {

        return toAjax(tcGoodsService.insertTcGoods(tcGoods));
    }

    /**
     * 修改物品信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcGoods:edit')")
    @Log(title = "物品信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TcGoods tcGoods) {

        return toAjax(tcGoodsService.updateTcGoods(tcGoods));
    }

    /**
     * 删除物品信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcGoods:remove')")
    @Log(title = "物品信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tcGoodsService.deleteTcGoodsByIds(ids));
    }
}
