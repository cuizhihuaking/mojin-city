package com.ruoyi.web.controller.tc;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.tc.domain.TcNav;
import com.ruoyi.tc.service.ITcNavService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 模块Controller
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@RestController
@RequestMapping("/tc/TcNav")
public class TcNavController extends BaseController {
    @Autowired
    private ITcNavService tcNavService;

    /**
     * 查询模块列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcNav:list')")
    @GetMapping("/list")
    public TableDataInfo list(TcNav tcNav) {
        startPage();
        List<TcNav> list = tcNavService.selectTcNavList(tcNav);
        return getDataTable(list);
    }

    /**
     * 导出模块列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcNav:export')")
    @Log(title = "模块", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TcNav tcNav) {
        List<TcNav> list = tcNavService.selectTcNavList(tcNav);
        ExcelUtil<TcNav> util = new ExcelUtil<TcNav>(TcNav.class);
        return util.exportExcel(list, "TcNav");
    }

    /**
     * 获取模块详细信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcNav:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tcNavService.selectTcNavById(id));
    }

    /**
     * 新增模块
     */
    @PreAuthorize("@ss.hasPermi('tc:TcNav:add')")
    @Log(title = "模块", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TcNav tcNav) {
        return toAjax(tcNavService.insertTcNav(tcNav));
    }

    /**
     * 修改模块
     */
    @PreAuthorize("@ss.hasPermi('tc:TcNav:edit')")
    @Log(title = "模块", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TcNav tcNav) {
        return toAjax(tcNavService.updateTcNav(tcNav));
    }

    /**
     * 删除模块
     */
    @PreAuthorize("@ss.hasPermi('tc:TcNav:remove')")
    @Log(title = "模块", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tcNavService.deleteTcNavByIds(ids));
    }
}
