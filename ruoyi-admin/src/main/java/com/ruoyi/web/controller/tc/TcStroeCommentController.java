package com.ruoyi.web.controller.tc;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.tc.domain.TcStroeComment;
import com.ruoyi.tc.service.ITcStroeCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 【请填写功能名称】Controller
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@RestController
@RequestMapping("/tc/TcStroeComment")
public class TcStroeCommentController extends BaseController {
    @Autowired
    private ITcStroeCommentService tcStroeCommentService;

    /**
     * 查询【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcStroeComment:list')")
    @GetMapping("/list")
    public TableDataInfo list(TcStroeComment tcStroeComment) {
        startPage();
        List<TcStroeComment> list = tcStroeCommentService.selectTcStroeCommentList(tcStroeComment);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcStroeComment:export')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TcStroeComment tcStroeComment) {
        List<TcStroeComment> list = tcStroeCommentService.selectTcStroeCommentList(tcStroeComment);
        ExcelUtil<TcStroeComment> util = new ExcelUtil<TcStroeComment>(TcStroeComment.class);
        return util.exportExcel(list, "TcStroeComment");
    }

    /**
     * 获取【请填写功能名称】详细信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcStroeComment:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tcStroeCommentService.selectTcStroeCommentById(id));
    }

    /**
     * 新增【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('tc:TcStroeComment:add')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TcStroeComment tcStroeComment) {
        return toAjax(tcStroeCommentService.insertTcStroeComment(tcStroeComment));
    }

    /**
     * 修改【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('tc:TcStroeComment:edit')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TcStroeComment tcStroeComment) {
        return toAjax(tcStroeCommentService.updateTcStroeComment(tcStroeComment));
    }

    /**
     * 删除【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('tc:TcStroeComment:remove')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tcStroeCommentService.deleteTcStroeCommentByIds(ids));
    }
}
