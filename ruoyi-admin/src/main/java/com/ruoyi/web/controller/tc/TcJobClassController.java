package com.ruoyi.web.controller.tc;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.tc.domain.TcJobClass;
import com.ruoyi.tc.service.ITcJobClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 职位分类Controller
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@RestController
@RequestMapping("/tc/TcJobClass")
public class TcJobClassController extends BaseController {
    @Autowired
    private ITcJobClassService tcJobClassService;

    /**
     * 查询职位分类列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcJobClass:list')")
    @GetMapping("/list")
    public TableDataInfo list(TcJobClass tcJobClass) {


        startPage();
        List<TcJobClass> list = tcJobClassService.selectTcJobClassList(tcJobClass);
        return getDataTable(list);
    }

    /**
     * 导出职位分类列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcJobClass:export')")
    @Log(title = "职位分类", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TcJobClass tcJobClass) {
        List<TcJobClass> list = tcJobClassService.selectTcJobClassList(tcJobClass);
        ExcelUtil<TcJobClass> util = new ExcelUtil<TcJobClass>(TcJobClass.class);
        return util.exportExcel(list, "TcJobClass");
    }

    /**
     * 获取职位分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcJobClass:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tcJobClassService.selectTcJobClassById(id));
    }

    /**
     * 新增职位分类
     */
    @PreAuthorize("@ss.hasPermi('tc:TcJobClass:add')")
    @Log(title = "职位分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TcJobClass tcJobClass) {
        return toAjax(tcJobClassService.insertTcJobClass(tcJobClass));
    }

    /**
     * 修改职位分类
     */
    @PreAuthorize("@ss.hasPermi('tc:TcJobClass:edit')")
    @Log(title = "职位分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TcJobClass tcJobClass) {
        return toAjax(tcJobClassService.updateTcJobClass(tcJobClass));
    }

    /**
     * 删除职位分类
     */
    @PreAuthorize("@ss.hasPermi('tc:TcJobClass:remove')")
    @Log(title = "职位分类", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tcJobClassService.deleteTcJobClassByIds(ids));
    }
}
