package com.ruoyi.web.controller.tc;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ConvertUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.tc.domain.TcQiugou;
import com.ruoyi.tc.service.ITcQiugouService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 求购Controller
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@RestController
@RequestMapping("/tc/TcQiugou")
public class TcQiugouController extends BaseController {
    @Autowired
    private ITcQiugouService tcQiugouService;

    /**
     * 查询求购列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcQiugou:list')")
    @GetMapping("/list")
    public TableDataInfo list(TcQiugou tcQiugou) {
        startPage();
        List<TcQiugou> list = tcQiugouService.selectTcQiugouList(tcQiugou);
        return getDataTable(list);
    }

    /**
     * 导出求购列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcQiugou:export')")
    @Log(title = "求购", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TcQiugou tcQiugou) {
        List<TcQiugou> list = tcQiugouService.selectTcQiugouList(tcQiugou);
        ExcelUtil<TcQiugou> util = new ExcelUtil<TcQiugou>(TcQiugou.class);
        return util.exportExcel(list, "TcQiugou");
    }

    /**
     * 获取求购详细信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcQiugou:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tcQiugouService.selectTcQiugouById(id));
    }

    /**
     * 新增求购
     */
    @PreAuthorize("@ss.hasPermi('tc:TcQiugou:add')")
    @Log(title = "求购", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TcQiugou tcQiugou) {
        return toAjax(tcQiugouService.insertTcQiugou(tcQiugou));
    }

    /**
     * 修改求购
     */
    @PreAuthorize("@ss.hasPermi('tc:TcQiugou:edit')")
    @Log(title = "求购", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TcQiugou tcQiugou) {
        return toAjax(tcQiugouService.updateTcQiugou(tcQiugou));
    }

    /**
     * 删除求购
     */
    @PreAuthorize("@ss.hasPermi('tc:TcQiugou:remove')")
    @Log(title = "求购", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tcQiugouService.deleteTcQiugouByIds(ids));
    }
}
