package com.ruoyi.web.controller.tc;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.tc.domain.TcCollect;
import com.ruoyi.tc.service.ITcCollectService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商品关注Controller
 *
 * @author é­éåå
 * @date 2022-01-23
 */
@RestController
@RequestMapping("/tc/TcCollect")
public class TcCollectController extends BaseController {
    @Autowired
    private ITcCollectService tcCollectService;

/**
 * 查询商品关注列表
 */
@PreAuthorize("@ss.hasPermi('tc:TcCollect:list')")
@GetMapping("/list")
        public TableDataInfo list(TcCollect tcCollect) {
        startPage();
        List<TcCollect> list = tcCollectService.selectTcCollectList(tcCollect);
        return getDataTable(list);
    }
    
    /**
     * 导出商品关注列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcCollect:export')")
    @Log(title = "商品关注", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TcCollect tcCollect) {
        List<TcCollect> list = tcCollectService.selectTcCollectList(tcCollect);
        ExcelUtil<TcCollect> util = new ExcelUtil<TcCollect>(TcCollect. class);
        return util.exportExcel(list, "TcCollect");
    }

    /**
     * 获取商品关注详细信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcCollect:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tcCollectService.selectTcCollectById(id));
    }

    /**
     * 新增商品关注
     */
    @PreAuthorize("@ss.hasPermi('tc:TcCollect:add')")
    @Log(title = "商品关注", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TcCollect tcCollect) {
        return toAjax(tcCollectService.insertTcCollect(tcCollect));
    }

    /**
     * 修改商品关注
     */
    @PreAuthorize("@ss.hasPermi('tc:TcCollect:edit')")
    @Log(title = "商品关注", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TcCollect tcCollect) {
        return toAjax(tcCollectService.updateTcCollect(tcCollect));
    }

    /**
     * 删除商品关注
     */
    @PreAuthorize("@ss.hasPermi('tc:TcCollect:remove')")
    @Log(title = "商品关注", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tcCollectService.deleteTcCollectByIds(ids));
    }
}
