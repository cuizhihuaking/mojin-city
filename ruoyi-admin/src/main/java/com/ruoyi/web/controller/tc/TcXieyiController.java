package com.ruoyi.web.controller.tc;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.tc.domain.TcXieyi;
import com.ruoyi.tc.service.ITcXieyiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 标签Controller
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@RestController
@RequestMapping("/tc/TcXieyi")
public class TcXieyiController extends BaseController {
    @Autowired
    private ITcXieyiService tcXieyiService;

    /**
     * 查询标签列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcXieyi:list')")
    @GetMapping("/list")
    public TableDataInfo list(TcXieyi tcXieyi) {
        startPage();
        List<TcXieyi> list = tcXieyiService.selectTcXieyiList(tcXieyi);
        return getDataTable(list);
    }

    /**
     * 导出标签列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcXieyi:export')")
    @Log(title = "标签", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TcXieyi tcXieyi) {
        List<TcXieyi> list = tcXieyiService.selectTcXieyiList(tcXieyi);
        ExcelUtil<TcXieyi> util = new ExcelUtil<TcXieyi>(TcXieyi.class);
        return util.exportExcel(list, "TcXieyi");
    }

    /**
     * 获取标签详细信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcXieyi:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tcXieyiService.selectTcXieyiById(id));
    }

    /**
     * 新增标签
     */
    @PreAuthorize("@ss.hasPermi('tc:TcXieyi:add')")
    @Log(title = "标签", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TcXieyi tcXieyi) {
        return toAjax(tcXieyiService.insertTcXieyi(tcXieyi));
    }

    /**
     * 修改标签
     */
    @PreAuthorize("@ss.hasPermi('tc:TcXieyi:edit')")
    @Log(title = "标签", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TcXieyi tcXieyi) {
        return toAjax(tcXieyiService.updateTcXieyi(tcXieyi));
    }

    /**
     * 删除标签
     */
    @PreAuthorize("@ss.hasPermi('tc:TcXieyi:remove')")
    @Log(title = "标签", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tcXieyiService.deleteTcXieyiByIds(ids));
    }
}
