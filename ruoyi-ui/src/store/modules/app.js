import Cookies from 'js-cookie'

const state = {
  sidebar: {
    opened: Cookies.get('sidebarStatus') ? !!+Cookies.get('sidebarStatus') : true,
    withoutAnimation: false
  },
  terminal: 3, // 画布选择的设备
    activeComponent: {}, // 选中模板数据
    componentsData: [], // 模板组件数据
  language: Cookies.get('language') || 'en',
  device: 'desktop',
  size: Cookies.get('size') || 'medium'
}

const mutations = {

  SET_TERMINAL: (state, terminal) => {
        state.terminal = terminal
        Cookies.set('terminal', terminal)
      },
      SET_ACTIVECOMPONENT: (state, activeComponent) => {
            state.activeComponent = activeComponent
            Cookies.set('activeComponent', activeComponent)
          },
          SET_COMPONENTSDATA: (state, componentsData) => {
                state.componentsData = componentsData
                Cookies.set('componentsData', componentsData)
              },
  TOGGLE_SIDEBAR: state => {
    state.sidebar.opened = !state.sidebar.opened
    state.sidebar.withoutAnimation = false
    if (state.sidebar.opened) {
      Cookies.set('sidebarStatus', 1)
    } else {
      Cookies.set('sidebarStatus', 0)
    }
  },
  SET_LANGUAGE: (state, language) => {
      state.language = language
      Cookies.set('language', language)
    },
  CLOSE_SIDEBAR: (state, withoutAnimation) => {
    Cookies.set('sidebarStatus', 0)
    state.sidebar.opened = false
    state.sidebar.withoutAnimation = withoutAnimation
  },
  TOGGLE_DEVICE: (state, device) => {
    state.device = device
  },
  SET_SIZE: (state, size) => {
    state.size = size
    Cookies.set('size', size)
  }
}

const actions = {
setTerminal({ commit }, terminal) {
    commit('SET_TERMINAL', terminal)
  },
  setActiveComponent({ commit }, activeComponent) {
      commit('SET_ACTIVECOMPONENT', activeComponent)
    },
    setComponentsData({ commit }, componentsData) {
        commit('SET_COMPONENTSDATA', componentsData)
      },
 setLanguage({ commit }, language) {
    commit('SET_LANGUAGE', language)
  },
  toggleSideBar({commit}) {
    commit('TOGGLE_SIDEBAR')
  },
  closeSideBar({commit}, {withoutAnimation}) {
    commit('CLOSE_SIDEBAR', withoutAnimation)
  },
  toggleDevice({commit}, device) {
    commit('TOGGLE_DEVICE', device)
  },
  setSize({commit}, size) {
    commit('SET_SIZE', size)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
