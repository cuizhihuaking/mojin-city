import Vue from 'vue'

import Cookies from 'js-cookie'

import Moment from 'moment'
import 'normalize.css/normalize.css' // a modern alternative to CSS resets
import Element from 'element-ui'
import './assets/styles/element-variables.scss'
import './assets/css/common.css'
import './assets/scss/index.scss'
import '@/assets/styles/index.scss' // global css
import '@/assets/styles/ruoyi.scss' // ruoyi css
import '@/styles/mojin.css' // 自定义 css
import App from './App'
import store from './store'
import router from './router'
import permission from './directive/permission'
import * as filters from './filters' // global filters
// register global utility filters
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})
import request from '@/utils/request'
import './assets/icons' // icon
import './permission' // permission control
import {getDicts} from "@/api/system/dict/data";
import {getConfigKey} from "@/api/system/config";
import {addDateRange, download, handleTree, parseTime, resetForm, selectDictLabel,selectDictLabels} from "@/utils/ruoyi";
import Pagination from "@/components/Pagination";
import Print from 'vue-print-nb'
import i18n from './lang'
Vue.use(Print);
import VueAMap from 'vue-amap';
Vue.use(VueAMap);
VueAMap.initAMapApiLoader({
  key: '106e6614c35c79d5d6b3386f7f93f3b6',
  plugin: ['AMap.PlaceSearch','AMap.Geolocation','AMap.Geocoder'],
  uiVersion: '1.0', // ui库版本，不配置不加载,
  v: '1.4.4'
});
import visibility from 'vue-visibility-change';
Vue.use(visibility);
// 全局方法挂载
Vue.prototype.getDicts = getDicts
Vue.prototype.getConfigKey = getConfigKey
Vue.prototype.parseTime = parseTime
Vue.prototype.resetForm = resetForm
Vue.prototype.addDateRange = addDateRange
Vue.prototype.selectDictLabel = selectDictLabel
Vue.prototype.selectDictLabels = selectDictLabels
Vue.prototype.download = download
Vue.prototype.handleTree = handleTree
Vue.prototype.$http = request // ajax请求方法
Vue.prototype.msgSuccess = function (msg) {
  this.$message({showClose: true, message: msg, type: "success"});
}

Vue.prototype.msgError = function (msg) {
  this.$message({showClose: true, message: msg, type: "error"});
}

Vue.prototype.msgInfo = function (msg) {
  this.$message.info(msg);
}

// 全局组件挂载
Vue.component('Pagination', Pagination)

Vue.use(permission)

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online! ! !
 */

Vue.use(Element, {
  size: Cookies.get('size') || 'medium' // set element-ui default size
})

Vue.config.productionTip = false
import VueClipboard from 'vue-clipboard2'
Vue.use(VueClipboard)
// use添加i18n
Vue.use(Element, {
  i18n: (key, value) => i18n.t(key, value)
})


new Vue({
  el: '#app',
  router,
  store,
  i18n,
  render: h => h(App)
})
