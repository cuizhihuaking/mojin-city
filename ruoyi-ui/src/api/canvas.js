export default (BSEURL) => {
  return {
    // 查询分类层级
    getClassify: BSEURL + '/canvas/getClassify',
    // 选择商品查询
    getProducts: BSEURL + '/canvas/getProducts',
    // 保存画布
    saveCanvas: BSEURL + '/sms/SmsStoreCanvas',
    // 读取画布
    getCanvas: BSEURL + '/sms/SmsStoreCanvas/11',
    // 选择店铺查询
    getShops: BSEURL + '/canvas/getShops'
  }
}
