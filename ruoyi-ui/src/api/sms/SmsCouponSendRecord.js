import request from '@/utils/request'

// 查询优惠卷卷码列表
export function listSmsCouponSendRecord (query) {
  return request({
    url: '/order/SmsCouponSendRecord/list',
    method: 'get',
    params: query
  })
}

// 查询优惠卷卷码详细
export function getSmsCouponSendRecord (id) {
  return request({
    url: '/order/SmsCouponSendRecord/' + id,
    method: 'get'
  })
}

// 新增优惠卷卷码
export function addSmsCouponSendRecord (data) {
  return request({
    url: '/order/SmsCouponSendRecord',
    method: 'post',
    data: data
  })
}

// 修改优惠卷卷码
export function updateSmsCouponSendRecord (data) {
  return request({
    url: '/order/SmsCouponSendRecord',
    method: 'put',
    data: data
  })
}

// 删除优惠卷卷码
export function delSmsCouponSendRecord (id) {
  return request({
    url: '/order/SmsCouponSendRecord/' + id,
    method: 'delete'
  })
}

// 导出优惠卷卷码
export function exportSmsCouponSendRecord (query) {
  return request({
    url: '/order/SmsCouponSendRecord/export',
    method: 'get',
    params: query
  })
}
