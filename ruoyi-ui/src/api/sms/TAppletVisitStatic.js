import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listTAppletVisitStatic(query) {
    return request({
        url: '/sms/TAppletVisitStatic/list',
        method: 'get',
        params: query
    })
}

// 查询【请填写功能名称】详细
export function getTAppletVisitStatic(id) {
    return request({
        url: '/sms/TAppletVisitStatic/' + id,
        method: 'get'
    })
}

// 新增【请填写功能名称】
export function addTAppletVisitStatic(data) {
    return request({
        url: '/sms/TAppletVisitStatic',
        method: 'post',
        data: data
    })
}

// 修改【请填写功能名称】
export function updateTAppletVisitStatic(data) {
    return request({
        url: '/sms/TAppletVisitStatic',
        method: 'put',
        data: data
    })
}

// 删除【请填写功能名称】
export function delTAppletVisitStatic(id) {
    return request({
        url: '/sms/TAppletVisitStatic/' + id,
        method: 'delete'
    })
}

// 导出【请填写功能名称】
export function exportTAppletVisitStatic(query) {
    return request({
        url: '/sms/TAppletVisitStatic/export',
        method: 'get',
        params: query
    })
}