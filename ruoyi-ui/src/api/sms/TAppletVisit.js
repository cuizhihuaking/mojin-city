import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listTAppletVisit(query) {
    return request({
        url: '/sms/TAppletVisit/list',
        method: 'get',
        params: query
    })
}

// 查询【请填写功能名称】详细
export function getTAppletVisit(id) {
    return request({
        url: '/sms/TAppletVisit/' + id,
        method: 'get'
    })
}

// 新增【请填写功能名称】
export function addTAppletVisit(data) {
    return request({
        url: '/sms/TAppletVisit',
        method: 'post',
        data: data
    })
}

// 修改【请填写功能名称】
export function updateTAppletVisit(data) {
    return request({
        url: '/sms/TAppletVisit',
        method: 'put',
        data: data
    })
}

// 删除【请填写功能名称】
export function delTAppletVisit(id) {
    return request({
        url: '/sms/TAppletVisit/' + id,
        method: 'delete'
    })
}

// 导出【请填写功能名称】
export function exportTAppletVisit(query) {
    return request({
        url: '/sms/TAppletVisit/export',
        method: 'get',
        params: query
    })
}