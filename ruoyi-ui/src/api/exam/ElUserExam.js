import request from '@/utils/request'

// 查询考试记录列表
export function listElUserExam (query) {
  return request({
    url: '/exam/ElUserExam/list',
    method: 'get',
    params: query
  })
}
export function pageing (query) {
  return request({
    url: '/exam/ElUserExam/pageing',
    method: 'get',
    params: query
  })
}


// 查询考试记录详细
export function getElUserExam (id) {
  return request({
    url: '/exam/ElUserExam/' + id,
    method: 'get'
  })
}

// 新增考试记录
export function addElUserExam (data) {
  return request({
    url: '/exam/ElUserExam',
    method: 'post',
    data: data
  })
}

// 修改考试记录
export function updateElUserExam (data) {
  return request({
    url: '/exam/ElUserExam',
    method: 'put',
    data: data
  })
}

// 删除考试记录
export function delElUserExam (id) {
  return request({
    url: '/exam/ElUserExam/' + id,
    method: 'delete'
  })
}

// 导出考试记录
export function exportElUserExam (query) {
  return request({
    url: '/exam/ElUserExam/export',
    method: 'get',
    params: query
  })
}
