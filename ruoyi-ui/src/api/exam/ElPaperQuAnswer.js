import request from '@/utils/request'

// 查询试卷考题备选答案列表
export function listElPaperQuAnswer (query) {
  return request({
    url: '/exam/ElPaperQuAnswer/list',
    method: 'get',
    params: query
  })
}

// 查询试卷考题备选答案详细
export function getElPaperQuAnswer (id) {
  return request({
    url: '/exam/ElPaperQuAnswer/' + id,
    method: 'get'
  })
}

// 新增试卷考题备选答案
export function addElPaperQuAnswer (data) {
  return request({
    url: '/exam/ElPaperQuAnswer',
    method: 'post',
    data: data
  })
}

// 修改试卷考题备选答案
export function updateElPaperQuAnswer (data) {
  return request({
    url: '/exam/ElPaperQuAnswer',
    method: 'put',
    data: data
  })
}

// 删除试卷考题备选答案
export function delElPaperQuAnswer (id) {
  return request({
    url: '/exam/ElPaperQuAnswer/' + id,
    method: 'delete'
  })
}

// 导出试卷考题备选答案
export function exportElPaperQuAnswer (query) {
  return request({
    url: '/exam/ElPaperQuAnswer/export',
    method: 'get',
    params: query
  })
}
