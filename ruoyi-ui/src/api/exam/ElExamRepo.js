import request from '@/utils/request'

// 查询考试题库列表
export function listElExamRepo (query) {
  return request({
    url: '/exam/ElExamRepo/list',
    method: 'get',
    params: query
  })
}

// 查询考试题库详细
export function getElExamRepo (id) {
  return request({
    url: '/exam/ElExamRepo/' + id,
    method: 'get'
  })
}

// 新增考试题库
export function addElExamRepo (data) {
  return request({
    url: '/exam/ElExamRepo',
    method: 'post',
    data: data
  })
}

// 修改考试题库
export function updateElExamRepo (data) {
  return request({
    url: '/exam/ElExamRepo',
    method: 'put',
    data: data
  })
}

// 删除考试题库
export function delElExamRepo (id) {
  return request({
    url: '/exam/ElExamRepo/' + id,
    method: 'delete'
  })
}

// 导出考试题库
export function exportElExamRepo (query) {
  return request({
    url: '/exam/ElExamRepo/export',
    method: 'get',
    params: query
  })
}
