import request from '@/utils/request'

// 查询候选答案列表
export function listElQuAnswer (query) {
  return request({
    url: '/exam/ElQuAnswer/list',
    method: 'get',
    params: query
  })
}

// 查询候选答案详细
export function getElQuAnswer (id) {
  return request({
    url: '/exam/ElQuAnswer/' + id,
    method: 'get'
  })
}

// 新增候选答案
export function addElQuAnswer (data) {
  return request({
    url: '/exam/ElQuAnswer',
    method: 'post',
    data: data
  })
}

// 修改候选答案
export function updateElQuAnswer (data) {
  return request({
    url: '/exam/ElQuAnswer',
    method: 'put',
    data: data
  })
}

// 删除候选答案
export function delElQuAnswer (id) {
  return request({
    url: '/exam/ElQuAnswer/' + id,
    method: 'delete'
  })
}

// 导出候选答案
export function exportElQuAnswer (query) {
  return request({
    url: '/exam/ElQuAnswer/export',
    method: 'get',
    params: query
  })
}
