import request from '@/utils/request'

// 查询课程列表
export function listElExam (query) {
  return request({
    url: '/exam/ElExam/list',
    method: 'get',
    params: query
  })
}
export function online (query) {
  return request({
    url: '/exam/ElExam/online',
    method: 'get',
    params: query
  })
}


// 查询课程详细
export function getElExam (id) {
  return request({
    url: '/exam/ElExam/' + id,
    method: 'get'
  })
}

// 新增课程
export function addElExam (data) {
  return request({
    url: '/exam/ElExam',
    method: 'post',
    data: data
  })
}

// 修改课程
export function updateElExam (data) {
  return request({
    url: '/exam/ElExam',
    method: 'put',
    data: data
  })
}

// 删除课程
export function delElExam (id) {
  return request({
    url: '/exam/ElExam/' + id,
    method: 'delete'
  })
}

// 导出课程
export function exportElExam (query) {
  return request({
    url: '/exam/ElExam/export',
    method: 'get',
    params: query
  })
}
