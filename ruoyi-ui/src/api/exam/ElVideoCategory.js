import request from '@/utils/request'

// 查询视频课程分类列表
export function listElVideoCategory(query) {
    return request({
        url: '/exam/ElVideoCategory/list',
        method: 'get',
        params: query
    })
}

// 查询视频课程分类详细
export function getElVideoCategory(id) {
    return request({
        url: '/exam/ElVideoCategory/' + id,
        method: 'get'
    })
}

// 新增视频课程分类
export function addElVideoCategory(data) {
    return request({
        url: '/exam/ElVideoCategory',
        method: 'post',
        data: data
    })
}

// 修改视频课程分类
export function updateElVideoCategory(data) {
    return request({
        url: '/exam/ElVideoCategory',
        method: 'put',
        data: data
    })
}

// 删除视频课程分类
export function delElVideoCategory(id) {
    return request({
        url: '/exam/ElVideoCategory/' + id,
        method: 'delete'
    })
}

// 导出视频课程分类
export function exportElVideoCategory(query) {
    return request({
        url: '/exam/ElVideoCategory/export',
        method: 'get',
        params: query
    })
}
