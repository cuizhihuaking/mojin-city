import request from '@/utils/request'

// 查询错题本列表
export function listElUserBook (query) {
  return request({
    url: '/exam/ElUserBook/list',
    method: 'get',
    params: query
  })
}
export function nextQu (query) {
  return request({
    url: '/exam/ElUserBook/next',
    method: 'get',
    params: query
  })
}
// 查询错题本详细
export function getElUserBook (id) {
  return request({
    url: '/exam/ElUserBook/' + id,
    method: 'get'
  })
}

// 新增错题本
export function addElUserBook (data) {
  return request({
    url: '/exam/ElUserBook',
    method: 'post',
    data: data
  })
}

// 修改错题本
export function updateElUserBook (data) {
  return request({
    url: '/exam/ElUserBook',
    method: 'put',
    data: data
  })
}

// 删除错题本
export function delElUserBook (id) {
  return request({
    url: '/exam/ElUserBook/' + id,
    method: 'delete'
  })
}

// 导出错题本
export function exportElUserBook (query) {
  return request({
    url: '/exam/ElUserBook/export',
    method: 'get',
    params: query
  })
}
