import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listTcStroeComment(query) {
    return request({
        url: '/tc/TcStroeComment/list',
        method: 'get',
        params: query
    })
}

// 查询【请填写功能名称】详细
export function getTcStroeComment(id) {
    return request({
        url: '/tc/TcStroeComment/' + id,
        method: 'get'
    })
}

// 新增【请填写功能名称】
export function addTcStroeComment(data) {
    return request({
        url: '/tc/TcStroeComment',
        method: 'post',
        data: data
    })
}

// 修改【请填写功能名称】
export function updateTcStroeComment(data) {
    return request({
        url: '/tc/TcStroeComment',
        method: 'put',
        data: data
    })
}

// 删除【请填写功能名称】
export function delTcStroeComment(id) {
    return request({
        url: '/tc/TcStroeComment/' + id,
        method: 'delete'
    })
}

// 导出【请填写功能名称】
export function exportTcStroeComment(query) {
    return request({
        url: '/tc/TcStroeComment/export',
        method: 'get',
        params: query
    })
}