import request from '@/utils/request'

// 查询职位分类列表
export function listTcJobClass(query) {
    return request({
        url: '/tc/TcJobClass/list',
        method: 'get',
        params: query
    })
}

// 查询职位分类详细
export function getTcJobClass(id) {
    return request({
        url: '/tc/TcJobClass/' + id,
        method: 'get'
    })
}

// 新增职位分类
export function addTcJobClass(data) {
    return request({
        url: '/tc/TcJobClass',
        method: 'post',
        data: data
    })
}

// 修改职位分类
export function updateTcJobClass(data) {
    return request({
        url: '/tc/TcJobClass',
        method: 'put',
        data: data
    })
}

// 删除职位分类
export function delTcJobClass(id) {
    return request({
        url: '/tc/TcJobClass/' + id,
        method: 'delete'
    })
}

// 导出职位分类
export function exportTcJobClass(query) {
    return request({
        url: '/tc/TcJobClass/export',
        method: 'get',
        params: query
    })
}