import request from '@/utils/request'

// 查询求购列表
export function listTcQiugou(query) {
    return request({
        url: '/tc/TcQiugou/list',
        method: 'get',
        params: query
    })
}

// 查询求购详细
export function getTcQiugou(id) {
    return request({
        url: '/tc/TcQiugou/' + id,
        method: 'get'
    })
}

// 新增求购
export function addTcQiugou(data) {
    return request({
        url: '/tc/TcQiugou',
        method: 'post',
        data: data
    })
}

// 修改求购
export function updateTcQiugou(data) {
    return request({
        url: '/tc/TcQiugou',
        method: 'put',
        data: data
    })
}

// 删除求购
export function delTcQiugou(id) {
    return request({
        url: '/tc/TcQiugou/' + id,
        method: 'delete'
    })
}

// 导出求购
export function exportTcQiugou(query) {
    return request({
        url: '/tc/TcQiugou/export',
        method: 'get',
        params: query
    })
}