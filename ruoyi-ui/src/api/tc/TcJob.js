import request from '@/utils/request'

// 查询招聘信息列表
export function listTcJob(query) {
    return request({
        url: '/tc/TcJob/list',
        method: 'get',
        params: query
    })
}

// 查询招聘信息详细
export function getTcJob(id) {
    return request({
        url: '/tc/TcJob/' + id,
        method: 'get'
    })
}

// 新增招聘信息
export function addTcJob(data) {
    return request({
        url: '/tc/TcJob',
        method: 'post',
        data: data
    })
}

// 修改招聘信息
export function updateTcJob(data) {
    return request({
        url: '/tc/TcJob',
        method: 'put',
        data: data
    })
}

// 删除招聘信息
export function delTcJob(id) {
    return request({
        url: '/tc/TcJob/' + id,
        method: 'delete'
    })
}

// 导出招聘信息
export function exportTcJob(query) {
    return request({
        url: '/tc/TcJob/export',
        method: 'get',
        params: query
    })
}
