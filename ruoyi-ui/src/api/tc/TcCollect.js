import request from '@/utils/request'

// 查询商品关注列表
export function listTcCollect(query) {
    return request({
        url: '/tc/TcCollect/list',
        method: 'get',
        params: query
    })
}

// 查询商品关注详细
export function getTcCollect(id) {
    return request({
        url: '/tc/TcCollect/' + id,
        method: 'get'
    })
}

// 新增商品关注
export function addTcCollect(data) {
    return request({
        url: '/tc/TcCollect',
        method: 'post',
        data: data
    })
}

// 修改商品关注
export function updateTcCollect(data) {
    return request({
        url: '/tc/TcCollect',
        method: 'put',
        data: data
    })
}

// 删除商品关注
export function delTcCollect(id) {
    return request({
        url: '/tc/TcCollect/' + id,
        method: 'delete'
    })
}

// 导出商品关注
export function exportTcCollect(query) {
    return request({
        url: '/tc/TcCollect/export',
        method: 'get',
        params: query
    })
}