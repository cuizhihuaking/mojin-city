import request from '@/utils/request'

// 查询商家店铺列表
export function listTcStoreInfo(query) {
    return request({
        url: '/tc/TcStoreInfo/list',
        method: 'get',
        params: query
    })
}

// 查询商家店铺详细
export function getTcStoreInfo(id) {
    return request({
        url: '/tc/TcStoreInfo/' + id,
        method: 'get'
    })
}

// 新增商家店铺
export function addTcStoreInfo(data) {
    return request({
        url: '/tc/TcStoreInfo',
        method: 'post',
        data: data
    })
}

// 修改商家店铺
export function updateTcStoreInfo(data) {
    return request({
        url: '/tc/TcStoreInfo',
        method: 'put',
        data: data
    })
}

// 删除商家店铺
export function delTcStoreInfo(id) {
    return request({
        url: '/tc/TcStoreInfo/' + id,
        method: 'delete'
    })
}

// 导出商家店铺
export function exportTcStoreInfo(query) {
    return request({
        url: '/tc/TcStoreInfo/export',
        method: 'get',
        params: query
    })
}