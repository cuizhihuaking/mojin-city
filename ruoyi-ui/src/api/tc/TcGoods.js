import request from '@/utils/request'

// 查询物品信息列表
export function listTcGoods(query) {
    return request({
        url: '/tc/TcGoods/list',
        method: 'get',
        params: query
    })
}

// 查询物品信息详细
export function getTcGoods(id) {
    return request({
        url: '/tc/TcGoods/' + id,
        method: 'get'
    })
}

// 新增物品信息
export function addTcGoods(data) {
    return request({
        url: '/tc/TcGoods',
        method: 'post',
        data: data
    })
}

// 修改物品信息
export function updateTcGoods(data) {
    return request({
        url: '/tc/TcGoods',
        method: 'put',
        data: data
    })
}

// 删除物品信息
export function delTcGoods(id) {
    return request({
        url: '/tc/TcGoods/' + id,
        method: 'delete'
    })
}

// 导出物品信息
export function exportTcGoods(query) {
    return request({
        url: '/tc/TcGoods/export',
        method: 'get',
        params: query
    })
}