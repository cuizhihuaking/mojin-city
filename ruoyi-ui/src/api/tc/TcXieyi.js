import request from '@/utils/request'

// 查询标签列表
export function listTcXieyi(query) {
    return request({
        url: '/tc/TcXieyi/list',
        method: 'get',
        params: query
    })
}

// 查询标签详细
export function getTcXieyi(id) {
    return request({
        url: '/tc/TcXieyi/' + id,
        method: 'get'
    })
}

// 新增标签
export function addTcXieyi(data) {
    return request({
        url: '/tc/TcXieyi',
        method: 'post',
        data: data
    })
}

// 修改标签
export function updateTcXieyi(data) {
    return request({
        url: '/tc/TcXieyi',
        method: 'put',
        data: data
    })
}

// 删除标签
export function delTcXieyi(id) {
    return request({
        url: '/tc/TcXieyi/' + id,
        method: 'delete'
    })
}

// 导出标签
export function exportTcXieyi(query) {
    return request({
        url: '/tc/TcXieyi/export',
        method: 'get',
        params: query
    })
}