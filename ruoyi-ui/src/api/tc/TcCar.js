import request from '@/utils/request'

// 查询房产信息列表
export function listTcCar(query) {
    return request({
        url: '/tc/TcCar/list',
        method: 'get',
        params: query
    })
}

// 查询房产信息详细
export function getTcCar(id) {
    return request({
        url: '/tc/TcCar/' + id,
        method: 'get'
    })
}

// 新增房产信息
export function addTcCar(data) {
    return request({
        url: '/tc/TcCar',
        method: 'post',
        data: data
    })
}

// 修改房产信息
export function updateTcCar(data) {
    return request({
        url: '/tc/TcCar',
        method: 'put',
        data: data
    })
}

// 删除房产信息
export function delTcCar(id) {
    return request({
        url: '/tc/TcCar/' + id,
        method: 'delete'
    })
}

// 导出房产信息
export function exportTcCar(query) {
    return request({
        url: '/tc/TcCar/export',
        method: 'get',
        params: query
    })
}