import request from '@/utils/request'

// 查询标签列表
export function listTcTag(query) {
    return request({
        url: '/tc/TcTag/list',
        method: 'get',
        params: query
    })
}

// 查询标签详细
export function getTcTag(id) {
    return request({
        url: '/tc/TcTag/' + id,
        method: 'get'
    })
}

// 新增标签
export function addTcTag(data) {
    return request({
        url: '/tc/TcTag',
        method: 'post',
        data: data
    })
}

// 修改标签
export function updateTcTag(data) {
    return request({
        url: '/tc/TcTag',
        method: 'put',
        data: data
    })
}

// 删除标签
export function delTcTag(id) {
    return request({
        url: '/tc/TcTag/' + id,
        method: 'delete'
    })
}

// 导出标签
export function exportTcTag(query) {
    return request({
        url: '/tc/TcTag/export',
        method: 'get',
        params: query
    })
}