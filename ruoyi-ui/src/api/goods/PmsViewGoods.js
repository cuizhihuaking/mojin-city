import request from '@/utils/request'

// 查询商品浏览记录列表
export function listPmsViewGoods(query) {
    return request({
        url: '/pms/PmsViewGoods/list',
        method: 'get',
        params: query
    })
}

// 查询商品浏览记录详细
export function getPmsViewGoods(userId) {
    return request({
        url: '/pms/PmsViewGoods/' + userId,
        method: 'get'
    })
}

// 新增商品浏览记录
export function addPmsViewGoods(data) {
    return request({
        url: '/pms/PmsViewGoods',
        method: 'post',
        data: data
    })
}

// 修改商品浏览记录
export function updatePmsViewGoods(data) {
    return request({
        url: '/pms/PmsViewGoods',
        method: 'put',
        data: data
    })
}

// 删除商品浏览记录
export function delPmsViewGoods(userId) {
    return request({
        url: '/pms/PmsViewGoods/' + userId,
        method: 'delete'
    })
}

// 导出商品浏览记录
export function exportPmsViewGoods(query) {
    return request({
        url: '/pms/PmsViewGoods/export',
        method: 'get',
        params: query
    })
}