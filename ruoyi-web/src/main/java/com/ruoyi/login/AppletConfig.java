package com.ruoyi.login;

public interface AppletConfig {

    public static String TITLE = "魔金同城";
    public static long defaultMemberLevel = 1;

    interface QQ {
        String APP_ID = "1111707307";
        String APP_SECRET = "uevhxYa7XUuQleqw";
        String AppToken = "678d3193a82f202eb5eb5f718de68d3e"; // 未使用
        String LOGINURL = "https://api.q.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code";

    }

    interface TOUTIAO {
        String APP_ID = "tt58542ee9c76cd93d01";
        String APP_SECRET = "c42984ee5f51b95327fb5972d4262671c9381b91";
        String LOGINURL = "https://developer.toutiao.com/api/apps/jscode2session?appid=%s&secret=%s&code=%s&grant_type=authorization_code";

    }

    interface ALIPAY {
        String privateKey = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCBtRXT6lU6GbNSBVkrBCS2ipblHJxjikZ8CDKAHEBF8oqa7h5IxNBEZpoyF3OXNTEm+TA+tkpv9nCP7l3viYBh0q7dQCBiKfHhguF1kVJprbCVFDFGaULVFlwhTq1A4waxuO28a1nNOlbcnZzYuCCisTPig7ZZ3TKtmB5/k1vJhG4ZTbPb/aYbDh8Ra8ouaZrtfy0/0/p/UUcDahqP8YKzmZYNkI4gy7q6tH/lChx7Hrd9MYCba7kEc2YUhgCIeaIYdn7Ue1afNq8jRJ9iIJWsLo5NlCRHcub76rSDVbITQyoq6p6Hbc6KhgsXq5pCcMjMgXVhcpoRtqKBRNgOsVJPAgMBAAECggEAQhh5Cwf6biTyj/VRvu3Zl3t/8ThnKQIpcMLsmQPMbNhvcQuuOv6v8msmYCg7Ku5cIbQRtaNGw08dhPq+u3WiCjdGDFDFL8CYFYaQRUzMZjWwKgekJHE3MUa3o/FnsA0AOEg4PS+nD0GPSdS3PGh3mPAbtG3R2nExhexifjglKcapL+wo+Q7DPL82SgOkt2J4iFNjo8by5XzRFASHih4kP62LHwiue6Rqrd2NnFXLO3xbhGzvCcre1KKsYoWHlUPMGDDRdTmm3SSB1FJcXUh5Mpk/EZvCm00x+gyvCnCLjYeV0k6pzRGbt8EJ48tB/J1DUG5TJfghOwkzsj2kaxYyAQKBgQDblJiX83ljXyqDUOPrVd33nlZDcypZWmMuxNL2zLfz41s0t4EWi0QQ/ToW5HbkZuNyMjhr0b3z/ir7KSyNqtg04EMm8wGcpkRPyKKJ+gFVhMfv3f/4M4bhq9aibJvVVejf3olHvEoSsh3Za/SvN+KUGtW5fG4RBk1ohT45kSyRDwKBgQCXOHhzt0ZhvMJwarEm2TnsYweVZHO8NzzYvIIjrlS04dSDJv7GcfhUU/eby5hXzgSToRHEvPhOFsxhb31OuVycIg4q+bSgztJOuoskllq/SYJi3iAk6PcsC47tJbkccKw5moES4OI7orYgCJT97iMiKTX2jKeDCoiqRjfvN/yqwQKBgAfpFYo72j6rOgPbESr0P/fgu2jYqEjpybLwok0D2QivQyTmC399aulVxhb3ChV2Y3FEv+Wae8ZjfLbpp9I+umVFQwYPMRqsnr9lEf34hL3I9877Z3JQoOZWY3gcg4yu+KHACaAM93ou8oZXrkv1rnL5eIOjKO/yEy3VxwUZ5sofAoGALkZqncaM9CXGPo53rZK15f+b93xo6kThCtYM4QxdAYlF9eh9+I4x1uRnFjbkZKI3xlF41YKaXZMecgajCu8wRtspCL+nJbqEovD02XplRLoi+0xL+9+Km+DhmPJ9RhW4rlv4eaV1tZgiMYVOyrIP8Ek6KTt7m18i2TBpt3ujh0ECgYEAjRIYfWc2t9ULzALSgHp0pdCtQm/9EGjNts9o8y/fOz6o85V1oCIB8BNUH3WkZaJUSmgmLq372ukSSUfcrzi0HxBn20MX/puaxljFuUIdQrb0uqhNVnOLjhRARIqBYBUgDu9ggLcGIXzAmF6lFolrTQ6/Uosw1jNKJ2GLpNEvJlQ=";
        String alipayPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgB/1YiKPR+D9ULOW2PmUW3PhtbbW3QTDrVGl5GigxAlgPU+ZfJ7EIXcG1IsXe/bOKg1AC8jsZNCAGDSojLpQmzibBc5/qM54S7KlfUpH7j123QaJWt9XeiK3m3G745JqAAxl2l5+CiHqkyfkdIEps26PQV/MfpLkppxeYNomOqIdvZE2z2xKr3UmaANqqnnqqczqGDVd/n32BFzwctiIAqz4NhqAJ7ZWWjSYHKXELFLiV8cRUkO5ebCxJeOCnS2QTs/nA1/BYgO2fb59kdEsqs9LXilbQHhfumQ/7J2Y2bBF522Cxx392c3y4rkxqPcMzHSXMHssrZwH/INt8LVvAQIDAQAB";
        String serverUrl = "https://openapi.alipay.com/gateway.do";
        String appId = "2021002140699739";
    }

    interface WECHATH5 {
        String appId = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCBtRXT6lU6GbNSBVkrBCS2ipblHJxjikZ8CDKAHEBF8oqa7h5IxNBEZpoyF3OXNTEm+TA+tkpv9nCP7l3viYBh0q7dQCBiKfHhguF1kVJprbCVFDFGaULVFlwhTq1A4waxuO28a1nNOlbcnZzYuCCisTPig7ZZ3TKtmB5/k1vJhG4ZTbPb/aYbDh8Ra8ouaZrtfy0/0/p/UUcDahqP8YKzmZYNkI4gy7q6tH/lChx7Hrd9MYCba7kEc2YUhgCIeaIYdn7Ue1afNq8jRJ9iIJWsLo5NlCRHcub76rSDVbITQyoq6p6Hbc6KhgsXq5pCcMjMgXVhcpoRtqKBRNgOsVJPAgMBAAECggEAQhh5Cwf6biTyj/VRvu3Zl3t/8ThnKQIpcMLsmQPMbNhvcQuuOv6v8msmYCg7Ku5cIbQRtaNGw08dhPq+u3WiCjdGDFDFL8CYFYaQRUzMZjWwKgekJHE3MUa3o/FnsA0AOEg4PS+nD0GPSdS3PGh3mPAbtG3R2nExhexifjglKcapL+wo+Q7DPL82SgOkt2J4iFNjo8by5XzRFASHih4kP62LHwiue6Rqrd2NnFXLO3xbhGzvCcre1KKsYoWHlUPMGDDRdTmm3SSB1FJcXUh5Mpk/EZvCm00x+gyvCnCLjYeV0k6pzRGbt8EJ48tB/J1DUG5TJfghOwkzsj2kaxYyAQKBgQDblJiX83ljXyqDUOPrVd33nlZDcypZWmMuxNL2zLfz41s0t4EWi0QQ/ToW5HbkZuNyMjhr0b3z/ir7KSyNqtg04EMm8wGcpkRPyKKJ+gFVhMfv3f/4M4bhq9aibJvVVejf3olHvEoSsh3Za/SvN+KUGtW5fG4RBk1ohT45kSyRDwKBgQCXOHhzt0ZhvMJwarEm2TnsYweVZHO8NzzYvIIjrlS04dSDJv7GcfhUU/eby5hXzgSToRHEvPhOFsxhb31OuVycIg4q+bSgztJOuoskllq/SYJi3iAk6PcsC47tJbkccKw5moES4OI7orYgCJT97iMiKTX2jKeDCoiqRjfvN/yqwQKBgAfpFYo72j6rOgPbESr0P/fgu2jYqEjpybLwok0D2QivQyTmC399aulVxhb3ChV2Y3FEv+Wae8ZjfLbpp9I+umVFQwYPMRqsnr9lEf34hL3I9877Z3JQoOZWY3gcg4yu+KHACaAM93ou8oZXrkv1rnL5eIOjKO/yEy3VxwUZ5sofAoGALkZqncaM9CXGPo53rZK15f+b93xo6kThCtYM4QxdAYlF9eh9+I4x1uRnFjbkZKI3xlF41YKaXZMecgajCu8wRtspCL+nJbqEovD02XplRLoi+0xL+9+Km+DhmPJ9RhW4rlv4eaV1tZgiMYVOyrIP8Ek6KTt7m18i2TBpt3ujh0ECgYEAjRIYfWc2t9ULzALSgHp0pdCtQm/9EGjNts9o8y/fOz6o85V1oCIB8BNUH3WkZaJUSmgmLq372ukSSUfcrzi0HxBn20MX/puaxljFuUIdQrb0uqhNVnOLjhRARIqBYBUgDu9ggLcGIXzAmF6lFolrTQ6/Uosw1jNKJ2GLpNEvJlQ=";
        String appSecret = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgB/1YiKPR+D9ULOW2PmUW3PhtbbW3QTDrVGl5GigxAlgPU+ZfJ7EIXcG1IsXe/bOKg1AC8jsZNCAGDSojLpQmzibBc5/qM54S7KlfUpH7j123QaJWt9XeiK3m3G745JqAAxl2l5+CiHqkyfkdIEps26PQV/MfpLkppxeYNomOqIdvZE2z2xKr3UmaANqqnnqqczqGDVd/n32BFzwctiIAqz4NhqAJ7ZWWjSYHKXELFLiV8cRUkO5ebCxJeOCnS2QTs/nA1/BYgO2fb59kdEsqs9LXilbQHhfumQ/7J2Y2bBF522Cxx392c3y4rkxqPcMzHSXMHssrZwH/INt8LVvAQIDAQAB";
        String merchantNum = "https://openapi.alipay.com/gateway.do";
        String apiKey = "2021002140699739";
    }

    interface WECHATAPP {
        String appId = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCBtRXT6lU6GbNSBVkrBCS2ipblHJxjikZ8CDKAHEBF8oqa7h5IxNBEZpoyF3OXNTEm+TA+tkpv9nCP7l3viYBh0q7dQCBiKfHhguF1kVJprbCVFDFGaULVFlwhTq1A4waxuO28a1nNOlbcnZzYuCCisTPig7ZZ3TKtmB5/k1vJhG4ZTbPb/aYbDh8Ra8ouaZrtfy0/0/p/UUcDahqP8YKzmZYNkI4gy7q6tH/lChx7Hrd9MYCba7kEc2YUhgCIeaIYdn7Ue1afNq8jRJ9iIJWsLo5NlCRHcub76rSDVbITQyoq6p6Hbc6KhgsXq5pCcMjMgXVhcpoRtqKBRNgOsVJPAgMBAAECggEAQhh5Cwf6biTyj/VRvu3Zl3t/8ThnKQIpcMLsmQPMbNhvcQuuOv6v8msmYCg7Ku5cIbQRtaNGw08dhPq+u3WiCjdGDFDFL8CYFYaQRUzMZjWwKgekJHE3MUa3o/FnsA0AOEg4PS+nD0GPSdS3PGh3mPAbtG3R2nExhexifjglKcapL+wo+Q7DPL82SgOkt2J4iFNjo8by5XzRFASHih4kP62LHwiue6Rqrd2NnFXLO3xbhGzvCcre1KKsYoWHlUPMGDDRdTmm3SSB1FJcXUh5Mpk/EZvCm00x+gyvCnCLjYeV0k6pzRGbt8EJ48tB/J1DUG5TJfghOwkzsj2kaxYyAQKBgQDblJiX83ljXyqDUOPrVd33nlZDcypZWmMuxNL2zLfz41s0t4EWi0QQ/ToW5HbkZuNyMjhr0b3z/ir7KSyNqtg04EMm8wGcpkRPyKKJ+gFVhMfv3f/4M4bhq9aibJvVVejf3olHvEoSsh3Za/SvN+KUGtW5fG4RBk1ohT45kSyRDwKBgQCXOHhzt0ZhvMJwarEm2TnsYweVZHO8NzzYvIIjrlS04dSDJv7GcfhUU/eby5hXzgSToRHEvPhOFsxhb31OuVycIg4q+bSgztJOuoskllq/SYJi3iAk6PcsC47tJbkccKw5moES4OI7orYgCJT97iMiKTX2jKeDCoiqRjfvN/yqwQKBgAfpFYo72j6rOgPbESr0P/fgu2jYqEjpybLwok0D2QivQyTmC399aulVxhb3ChV2Y3FEv+Wae8ZjfLbpp9I+umVFQwYPMRqsnr9lEf34hL3I9877Z3JQoOZWY3gcg4yu+KHACaAM93ou8oZXrkv1rnL5eIOjKO/yEy3VxwUZ5sofAoGALkZqncaM9CXGPo53rZK15f+b93xo6kThCtYM4QxdAYlF9eh9+I4x1uRnFjbkZKI3xlF41YKaXZMecgajCu8wRtspCL+nJbqEovD02XplRLoi+0xL+9+Km+DhmPJ9RhW4rlv4eaV1tZgiMYVOyrIP8Ek6KTt7m18i2TBpt3ujh0ECgYEAjRIYfWc2t9ULzALSgHp0pdCtQm/9EGjNts9o8y/fOz6o85V1oCIB8BNUH3WkZaJUSmgmLq372ukSSUfcrzi0HxBn20MX/puaxljFuUIdQrb0uqhNVnOLjhRARIqBYBUgDu9ggLcGIXzAmF6lFolrTQ6/Uosw1jNKJ2GLpNEvJlQ=";
        String appSecret = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgB/1YiKPR+D9ULOW2PmUW3PhtbbW3QTDrVGl5GigxAlgPU+ZfJ7EIXcG1IsXe/bOKg1AC8jsZNCAGDSojLpQmzibBc5/qM54S7KlfUpH7j123QaJWt9XeiK3m3G745JqAAxl2l5+CiHqkyfkdIEps26PQV/MfpLkppxeYNomOqIdvZE2z2xKr3UmaANqqnnqqczqGDVd/n32BFzwctiIAqz4NhqAJ7ZWWjSYHKXELFLiV8cRUkO5ebCxJeOCnS2QTs/nA1/BYgO2fb59kdEsqs9LXilbQHhfumQ/7J2Y2bBF522Cxx392c3y4rkxqPcMzHSXMHssrZwH/INt8LVvAQIDAQAB";
        String merchantNum = "https://openapi.alipay.com/gateway.do";
        String apiKey = "2021002140699739";
    }

    interface WECHATAPPLET {
        // String appId = "wx6007249d6b2348ea";
        // String appSecret = "1b1c5e732f38a03c980061855e68a19a";
        String appId = "wx1ae920b92c8914cd";
        String appSecret = "aa218808eda52b7dbbc1288c569c5d7c";
        String merchantNum = "1487327342";
        String apiKey = "wJlIo09cibswnaghisLiI9865gJ76lJy";
        String payCallback = "https://mojin.51wangshi.com/api-web/tc/openvipNotice";
        String publishNotice = "https://mojin.51wangshi.com/api-web/tc/publishNotice";

    }
}
