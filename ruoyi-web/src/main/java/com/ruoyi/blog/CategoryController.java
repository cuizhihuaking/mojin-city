package com.ruoyi.blog;


import com.ruoyi.cms.bean.ArticleList;
import com.ruoyi.cms.bean.ColumnList;
import com.ruoyi.cms.service.ArticleListService;
import com.ruoyi.cms.service.ColumnListService;
import com.ruoyi.common.annotation.UnAuth;
import com.ruoyi.util.PageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by GeneratorFx on 2017-04-11.
 */
@Controller
@RequestMapping("/categories")
public class CategoryController {

    @Resource
    private ColumnListService partnerService;

    @Resource
    private ArticleListService articleService;
    @Resource
    private ColumnListService categoryService;
    @Resource
    private ColumnListService tagService;

    /**
     * 获取某个标签的分页文章
     *
     * @param model
     * @param pager
     * @param categoryId
     * @return
     */
    @UnAuth
    @RequestMapping("/load/{categoryId}")
    public String loadArticleByCategory(PageHelper<ArticleList> pageHelper, Model model, Pager pager, @PathVariable Integer categoryId) {
        pager.getStart();
        List<ArticleList> articleList = articleService.queryArticleList(pageHelper, null, categoryId, -1L).getList();
        if (!articleList.isEmpty()) {
            model.addAttribute("articleList", articleList);
            model.addAttribute("pager", pager);
            model.addAttribute("categoryName", articleList.get(0).getColumnName());
        }
        return "blog/part/categorySummary";
    }

    /**
     * 跳转到分类的页面 暂时停用
     *
     * @param model
     * @param categoryId
     * @return
     */
    @UnAuth
    @Deprecated
    @RequestMapping("/details/{categoryId}")
    public String categoryPage(Model model, @PathVariable Integer categoryId) {
        List<ColumnList> partnerList = partnerService.queryColumnList();
        model.addAttribute("partnerList", partnerList);
        model.addAttribute("categoryId", categoryId);
        return "category";
    }


}
