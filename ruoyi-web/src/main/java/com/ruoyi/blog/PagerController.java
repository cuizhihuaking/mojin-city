package com.ruoyi.blog;


import com.ruoyi.cms.service.ArticleListService;
import com.ruoyi.cms.service.ColumnListService;
import com.ruoyi.common.annotation.UnAuth;
import com.ruoyi.util.CommonConstant;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


/**
 * @author Do
 * @package com.魔金.zblog.controller.admin
 * @name PagerController
 * @date 2017/4/11
 * @time 21:44
 */
@RestController
public class PagerController {

    @Resource
    private ColumnListService partnerService;

    @Resource
    private ArticleListService articleService;
    @Resource
    private ColumnListService categoryService;
    @Resource
    private ColumnListService tagService;

    /**
     * 初始化文章分页信息
     *
     * @return
     */
    @UnAuth
    @RequestMapping("/pager/articles/load")
    public Pager loadArticlePager(Pager pager) {
        pager.setTotalCount(articleService.queryArticleListCount(null, -1, CommonConstant.QUERY_WITH_ISRELEASE));
        return pager;
    }

    /**
     * 初始化当前分类id的文章分页信息
     *
     * @param pager      分页对象
     * @param categoryId 分类id
     * @return
     */
    @UnAuth
    @RequestMapping("/pager/categories/{categoryId}")
    public Pager loadCategoryPager(Pager pager, @PathVariable Integer categoryId) {
        pager.setTotalCount(articleService.queryArticleListCount(null, categoryId, CommonConstant.QUERY_WITH_ISRELEASE));
        return pager;
    }

    /**
     * 初始化当前标签的文章分页信息
     *
     * @param pager 分页对象
     * @param tagId 标签
     * @return
     */
    @UnAuth
    @RequestMapping("/pager/tags/{tagId}")
    @ResponseBody
    public Pager initPage(Pager pager, @PathVariable Integer tagId) {
        pager.setTotalCount(5);
        return pager;
    }

}
