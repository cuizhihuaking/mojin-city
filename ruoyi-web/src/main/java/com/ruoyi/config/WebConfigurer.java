/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:WebConfigurer.java
 * Date:2020/09/13 08:43:13
 */

package com.ruoyi.config;

import com.ruoyi.interceptor.AuthorityInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Created by 魔金商城 on 2019/5/13.
 * 拦截器配置
 */
@Configuration
public class WebConfigurer implements WebMvcConfigurer {

    /**
     * 注入登录拦截器
     */
    @Autowired
    private AuthorityInterceptor authorityInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authorityInterceptor).
                excludePathPatterns("/templates/**", "/static/**", "/v2/**", "/swagger-ui.html",
                        "/webjars/**", "/swagger-resources/**", "/**/*.css", "/**/*.js", "/**/*.png",
                        "/**/*.jpg", "/**/*.jpeg", "/**/*.gif", "/**/fonts/*", "/**/*.svg", "/**/*.ico", "/**/*.map"
                );
    }


    /**
     * 将static下面的js，css文件加载出来
     *
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
    }
}
