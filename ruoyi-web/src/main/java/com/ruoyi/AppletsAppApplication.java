/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:AppletsAppApplication.java
 * Date:2020/11/27 16:26:27
 */

package com.ruoyi;


import com.ruoyi.common.core.redis.RedisCacheErrorHandler;
import com.ruoyi.common.utils.SnowflakeIdWorker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cache.interceptor.CacheErrorHandler;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by 魔金商城 on 2019/5/1.
 * 小程序app后端启动类
 */
@Slf4j
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableTransactionManagement
public class AppletsAppApplication {

    public static void main(String[] args) throws UnknownHostException {
        System.out.println("start-------------");
        // SpringApplication.run(MallPortalApplication.class, args);
        ConfigurableApplicationContext application = SpringApplication.run(AppletsAppApplication.class, args);
        Environment env = application.getEnvironment();
        String ip = InetAddress.getLocalHost().getHostAddress();
        String port = env.getProperty("server.port");
         /*  BufferedReader br = null;
        String path = "jobclass.json";
        try {
            br = new BufferedReader(new InputStreamReader(T.class.getClassLoader().getResourceAsStream(path)));
            String line = br.readLine();
            StringBuilder sb = new StringBuilder();
            while (line != null) {
                sb.append(line + "\r\n");
                line = br.readLine();
            }
            ITcJobClassService tcJobClassService = SpringUtils.getBean(ITcJobClassService.class);
            List<TcJobClassBak> list1 = JSONArray.parseArray(sb.toString(), TcJobClassBak.class);
            for (TcJobClassBak tag:list1){
                TcJobClass jobClass = new TcJobClass();
                jobClass.setId(tag.getId());
                jobClass.setAllName(tag.getFull_name());
                jobClass.setAllPinyinName(tag.getFull_list_name());
                jobClass.setCode(tag.getCode());
                jobClass.setName(tag.getName());
                jobClass.setParentCode(tag.getParent_code());
                tcJobClassService.insertTcJobClass(jobClass);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedReader br = null;
        String path = "nav.json";
        try {
            br = new BufferedReader(new InputStreamReader(T.class.getClassLoader().getResourceAsStream(path)));
            String line = br.readLine();
            StringBuilder sb = new StringBuilder();
            while (line != null) {
                sb.append(line + "\r\n");
                line = br.readLine();
            }
            ITcNavService tcJobClassService = SpringUtils.getBean(ITcNavService.class);
            List<TcNavBak> list1 = JSONArray.parseArray(sb.toString(), TcNavBak.class);
            for (TcNavBak tag:list1){
                TcNav jobClass = new TcNav();
                jobClass.setId(tag.getId());
                jobClass.setPic(tag.getImage());
                jobClass.setStatus(tag.getStatus());
                jobClass.setName(tag.getName());
                jobClass.setPid(tag.getPid());
                jobClass.setType(tag.getType());
                jobClass.setUrl(tag.getPath());
                jobClass.setWeight(tag.getWeigh());
                tcJobClassService.insertTcNav(jobClass);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        log.info("\n----------------------------------------------------------\n\t" +
                "Application mojin-Boot is running! Access URLs:\n\t" +
                "Local: \t\thttp://localhost:" + port + "/\n\t" +
                "External: \thttp://" + ip + ":" + port + "/\n\t" +
                "swagger-ui: \thttp://" + ip + ":" + port + "/swagger-ui.html\n\t" +

                "-----------------页面请部署 ruoyi-uniapp-----------------------------------------");
    }

    /**
     * redis 异常处理
     */
    @Bean
    public CacheErrorHandler errorHandler() {
        return new RedisCacheErrorHandler();
    }

    /**
     * 序列号生成器
     */
    @Bean
    public SnowflakeIdWorker snowflakeIdWorker() {
        SnowflakeIdWorker snowflakeIdWorker = new SnowflakeIdWorker(0, 1);
        return snowflakeIdWorker;
    }


}
