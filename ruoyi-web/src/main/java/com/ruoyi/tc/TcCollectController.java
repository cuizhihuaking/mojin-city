/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:AttentionController.java
 * Date:2020/11/27 16:26:27
 */

package com.ruoyi.tc;

import com.ruoyi.appletsutil.AppletsLoginUtils;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.tc.domain.TcCollect;
import com.ruoyi.tc.service.ITcCollectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * Created by 魔金商城 on 18/7/3
 * 商品关注控制器
 */
@Slf4j
@Api(description = "商品关注接口")
@RestController
public class TcCollectController extends BaseController {

    /**
     * 注入商品关注服务接口
     */
    @Autowired
    private ITcCollectService attentionService;


    /**
     * 查询用户收藏的单品信息
     *
     * @param tagParam 分页帮助类
     * @return 返回用户收藏的单品信息
     */
    @RequestMapping(value = "/tc/queryCollect")
    @ResponseBody
    @ApiOperation(value = "查询用户收藏的单品信息", notes = "查询用户收藏的单品信息(需要认证)", httpMethod = "POST")
    public TableDataInfo tuiguangList(TcCollect tagParam, HttpServletRequest request) {
        tagParam.setCustomerId(AppletsLoginUtils.getInstance().getCustomerId(request));
        startPage();
        List<TcCollect> tcTagList = attentionService.selectTcCollectList(tagParam);
        return getDataTables(tcTagList);
    }

    /**
     * 添加关注
     *
     * @return 成功返回1 失败返回0
     */
    @RequestMapping(value = "/tc/addcollect")
    @ResponseBody
    public AjaxResult addAttention(HttpServletRequest request, @RequestBody TcCollect collect) {
        if (collect.getId() != null) {
            return AjaxResult.success(attentionService.deleteTcCollectById(collect.getId()));
        }
        collect.setCreateTime(new Date());
        collect.setCustomerId(AppletsLoginUtils.getInstance().getCustomerId(request));
        return AjaxResult.success(attentionService.insertTcCollect(collect));
    }

    /**
     * 是否收藏
     *
     * @return 返回分类集合
     */
    @GetMapping("/tc/isCollect")//
    @Log(title = "查询一级二级三级分类", businessType = BusinessType.SELECT)
    public AjaxResult roomDetail(@RequestParam(value = "objId", required = true) long objId,
                                 @RequestParam(value = "type", required = true) String type) {
        Long t = System.currentTimeMillis();
        try {
            log.debug("房屋详情......start");
            TcCollect collect = new TcCollect();
            collect.setObjId(objId);
            collect.setType(type);
            List<TcCollect> list = attentionService.selectTcCollectList(collect);
            if (list != null && list.size() > 0) {
                return AjaxResult.success(list.get(0));
            }

        } catch (Exception e) {
            log.debug("房屋详情......end" + (System.currentTimeMillis() - t) + "ms");
            return AjaxResult.error(e.getMessage());
        } finally {
            log.debug("房屋详情......end" + (System.currentTimeMillis() - t) + "ms");
        }
        return AjaxResult.success();
    }
}
