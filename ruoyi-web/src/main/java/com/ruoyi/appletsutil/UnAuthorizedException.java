/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:UnAuthorizedException.java
 * Date:2020/09/13 08:43:13
 */

package com.ruoyi.appletsutil;

import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.md5.MessageSourceUtil;
import lombok.Data;

/**
 * Created by 魔金商城 on 2018/6/13.
 * 微信小程序未授权异常
 */
@Data
public class UnAuthorizedException extends ServiceException {

    /**
     * 错误code
     */
    private String errorCode;

    public UnAuthorizedException(String errorCode) {
        this(errorCode, MessageSourceUtil.getMessage(errorCode));
    }

    public UnAuthorizedException(String errorCode, String message) {
        super(message == null ? errorCode : message);
        this.errorCode = errorCode;
    }
}
