## 系统需求

 原创地址 https://mp.weixin.qq.com/s/DpJe6XpH2maIKnzfNa-_YQ

[后台管理](https://mojin.51wangshi.com/admin/login?redirect=%2Findex) admin 123456

 **小程序演示** 

![输入图片说明](%E5%B0%8F%E7%A8%8B%E5%BA%8F.png)

* JDK >= 1.8

* MySQL >= 5.5

* Maven >= 3.0

## 技术选型

* 1、系统环境



Java EE 8

Servlet 3.0

Apache Maven 3

* 2、主框架



* Spring Boot 2.1

* Spring Framework 5.1

* Spring Security 5.1

3、持久层



* Apache MyBatis 3.4

* Hibernate Validation 6.0

* Alibaba Druid 1.1

* 4、视图层



* Vue 2.6

* Axios 0.18

* Element UI 2.11

*  5.小程序使用uniapp（https://uniapp.dcloud.io/）



# 环境部署

# 准备工作

* JDK >= 1.8 (推荐1.8版本)

* Mysql >= 5.5.0 (推荐5.7版本)

* Redis >= 3.0

* Maven >= 3.0

* Node >= 10

提示



* 前端安装完node后，最好设置下淘宝的镜像源，不建议使用cnpm（可能会出现奇怪的问题）





# 后端运行

* 1、导入ruoyi到Eclipse，菜单 File -> Import，然后选择 Maven -> Existing Maven Projects，点击 Next> 按钮，选择工作目录，然后点击 Finish 按钮，即可成功导入Eclipse会自动加载Maven依赖包，初次加载会比较慢（根据自身网络情况而定）

* 2、创建数据库mojin并导入数据脚本mojin.sql

* 3.修改好各自项目中的application-dev.yml中的数据库配置和redis配置

* 4、打开运行com.ruoyi.RuoYiApplication.java（平台管理接口，前端ruoyi-ui【vue项目】）

* 5、打开运行com.ruoyi.RuoYiStoreApplication.java（商户管理接口，前端ruoyi-store-ui【vue项目】）

* 5、打开运行com.ruoyi.AppletsAppApplication.java（用户端接口，前端ruoyi-uniApp模块【uniapp项目】）



# 前端运行



# 进入项目目录

* cd ruoyi-ui



# 安装依赖

* npm install



# 强烈建议不要用直接使用 cnpm 安装，会有各种诡异的 bug，可以通过重新指定 registry 来解决 npm 安装速度慢的问题。

* npm install --registry=https://registry.npm.taobao.org



# 本地开发 启动项目

* npm run dev



* 当项目开发完毕，只需要运行一行命令就可以打包你的应用



# 打包正式环境

* npm run build:prod



### 用户端uniapp（ruoyi-uniApp模块）



1. 下载编辑器HBuilderX.

1. 下载微信开发者工具.

1. 点击HBuilderX顶部菜单->文件->导入->从git导入.

1. 鼠标点击App.vue获取焦点,无需打开文件.

1. 点击HBuilderX编辑器顶部菜单->运行->运行到小程序模拟器.

1. 如果启动不了微信开发者工具，请手动启动微信开发者工具，手动添加项目(项目路径为unpackage/dev/mp-weixin)

1. 打 包: 7.1 打包app：点击HBuilderX顶部导航->发行->原生APP云打包. 7.2 打包微信小程序：把项目路径unpackage/dev/mp-weixin文件夹拷贝出来即可。

1. 修改接口地址

需要修改config/index.config.js 中的 baseUrl:  改为ruoyi-web中配置的接口地址

其他配置 如appid等需要修改manifest.json(用hbuild打开 有编辑器模式)







# 常见问题

* 如果使用Mac 需要修改application.yml文件路径profile

* 如果使用Linux 提示表不存在，设置大小写敏感配置在/etc/my.cnf 添加lower_case_table_names=1，重启MYSQL服务

* 如果提示当前权限不足，无法写入文件请检查profile是否可读可写，或者无法访问此目录



# 内置功能

## 平台简介



* 前端采用Vue、Element UI。

* 后端采用Spring Boot、Spring Security、Redis & Jwt。

* 权限认证使用Jwt，支持多终端认证系统。

* 支持加载动态权限菜单，多方式轻松权限控制。

* 高效率开发，使用代码生成器可以一键生成前后端代码。

## 内置功能



1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。

2.  部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。

3.  岗位管理：配置系统用户所属担任职务。

4.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。

5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。

6.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。

7.  参数管理：对系统动态配置常用参数。

8.  通知公告：系统通知公告信息发布维护。

9.  操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。

10. 登录日志：系统登录日志记录查询包含登录异常。

11. 在线用户：当前系统中活跃用户状态监控。

12. 定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。

13. 代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。

14. 系统接口：根据业务代码自动生成相关的api接口文档。

15. 服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。

16. 在线构建器：拖动表单元素生成相应的HTML代码。

17. 连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。

18.协议管理 （tc_xieyi）

图片

图片

19.广告管理(tc_adv)

图片

20.商家店铺(tc_store_info)

图片



图片

图片

图片

21.会员等级(tc_member_level)

图片

图片

22.招聘信息(tc_job)

图片

图片

23.物品交易（tc_goods）

图片

图片

24.农林牧渔信息(tc_nonglin)

图片

图片

25.求购信息(tc_qiugou)

图片

图片

图片

26.房产信息(tc_room) 出租和出售

图片

图片

27.推广信息(tc_tuiguang)

图片

图片

28.人才简历（tc_user_card）

图片

图片

图片

29.转让信息(tc_zhuanrang)

图片

图片

主页

图片

图片





魔金同城体验微信群 喜欢的朋友可以添加
 **此项目仅供学习 请勿商用，如需商用请加微信授权后 方可商用** 
![输入图片说明](image.png)
![输入图片说明](qun.png)
