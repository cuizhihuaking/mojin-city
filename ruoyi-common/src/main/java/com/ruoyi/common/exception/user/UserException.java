/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:UserException.java
 * Date:2020/09/13 08:43:13
 */

package com.ruoyi.common.exception.user;

import com.ruoyi.common.exception.BaseException;

/**
 * 用户信息异常类
 *
 * @author ruoyi
 */
public class UserException extends BaseException {
    private static final long serialVersionUID = 1L;

    public UserException(String code, Object[] args) {
        super("user", code, args, null);
    }
}
