/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:FileException.java
 * Date:2020/09/13 08:43:13
 */

package com.ruoyi.common.exception.file;

import com.ruoyi.common.exception.BaseException;

/**
 * 文件信息异常类
 *
 * @author ruoyi
 */
public class FileException extends BaseException {
    private static final long serialVersionUID = 1L;

    public FileException(String code, Object[] args) {
        super("file", code, args, null);
    }

}
