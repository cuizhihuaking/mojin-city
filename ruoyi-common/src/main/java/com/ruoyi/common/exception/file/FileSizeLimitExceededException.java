/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:FileSizeLimitExceededException.java
 * Date:2020/09/13 08:43:13
 */

package com.ruoyi.common.exception.file;

/**
 * 文件名大小限制异常类
 *
 * @author ruoyi
 */
public class FileSizeLimitExceededException extends FileException {
    private static final long serialVersionUID = 1L;

    public FileSizeLimitExceededException(long defaultMaxSize) {
        super("upload.exceed.maxSize", new Object[]{defaultMaxSize});
    }
}
