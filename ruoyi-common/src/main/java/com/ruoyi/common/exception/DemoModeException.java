/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:DemoModeException.java
 * Date:2020/09/13 08:43:13
 */

package com.ruoyi.common.exception;

/**
 * 演示模式异常
 *
 * @author ruoyi
 */
public class DemoModeException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public DemoModeException() {
    }
}
