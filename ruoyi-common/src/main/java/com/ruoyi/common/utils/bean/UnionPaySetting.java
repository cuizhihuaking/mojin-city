/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:UnionPaySetting.java
 * Date:2020/09/13 08:43:13
 */

package com.ruoyi.common.utils.bean;

import lombok.Data;

/**
 * 银联支付设置实体
 */
@Data
public class UnionPaySetting {
    /**
     * 商户号
     */
    private String merchantNum;
    /**
     * 前台回调地址
     */
    private String beforeCallbackUrl;
    /**
     * 后台回调地址
     */
    private String backCallbackUrl;

}
