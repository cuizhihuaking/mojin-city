/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:WechatParam.java
 * Date:2021/01/09 11:40:09
 */

package com.ruoyi.common.utils;

import lombok.Data;

@Data
public class WechatParam {
    private String begin_date;
    private String end_date;
}
