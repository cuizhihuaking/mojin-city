/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:LineColor.java
 * Date:2020/09/13 08:43:13
 */

package com.ruoyi.common.utils.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 颜色实体类
 *
 * @author 魔金商城 created on 2020/4/14
 */
@Data
@ApiModel(description = "颜色实体")
public class LineColor {

    /**
     * 红色
     */
    @ApiModelProperty(value = "红色")
    private int r;

    /**
     * 绿色
     */
    @ApiModelProperty(value = "绿色")
    private int g;

    /**
     * 蓝色
     */
    @ApiModelProperty(value = "蓝色")
    private int b;

}
