/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:WechatCodeSetting.java
 * Date:2021/01/11 21:00:11
 */

package com.ruoyi.common.utils.bean;

import lombok.Data;

/**
 * 微信设置实体类
 */
@Data
public class WechatCodeSetting {
    /**
     * 公众号appId(必传)
     */
    private String component_appid;
    /**
     * AppSecret(必传)
     */
    private String component_appsecret;
    /**
     * 退款证书
     */
    private String component_verify_ticket;


}
