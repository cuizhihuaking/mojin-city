/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:QqOssClient.java
 * Date:2020/11/27 16:26:27
 */

package com.ruoyi.common.utils;


import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.model.PutObjectRequest;
import com.qcloud.cos.model.PutObjectResult;
import com.qcloud.cos.region.Region;
import com.ruoyi.common.md5.MD5;
import com.ruoyi.common.utils.bean.OssYunConf;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * @Author: SunJunxian
 * @Date: 2018/12/17
 * @Description: java实现腾讯云存储服务（COSClient）
 * @REGIONID 区域
 * @KEY 上传上云之后的名字
 * @KEY01 需要删除的文件
 */
public class QqOssClient {


    /**
     * 上传腾讯云
     *
     * @param file
     * @param bytes 图片字节
     * @param type  上传的类型 0 图片 1 视频
     * @return 返回图片在又拍云的地址
     */
    public static String uploadToQqOss(OssYunConf cloudStorageConfig, MultipartFile file, byte[] bytes, String fileOriginName, String type) {
        String url = null;

        String accessKey = cloudStorageConfig.getAccessKeyId();
        String secretKey = cloudStorageConfig.getAccessKeySecret();
        String bucket = cloudStorageConfig.getEndPoint();
        // bucket的命名规则为{name}-{appid} ，此处填写的存储桶名称必须为此格式
        String bucketName = cloudStorageConfig.getBucketName();
        // 1 初始化用户身份信息(secretId, secretKey)
        COSCredentials cred = new BasicCOSCredentials(accessKey, secretKey);
        // 2 设置bucket的区域, COS地域的简称请参照 https://cloud.tencent.com/document/product/436/6224
        ClientConfig clientConfig = new ClientConfig(new Region(bucket));
        // 3 生成cos客户端
        COSClient cosclient = new COSClient(cred, clientConfig);
        //  String suffix = fileOriginName.substring(fileOriginName.lastIndexOf(".")).toLowerCase();
        //获取腾讯云路径前缀
        String dir = cloudStorageConfig.getPrefix();

        // 简单文件上传, 最大支持 5 GB, 适用于小文件上传, 建议 20 M 以下的文件使用该接口
        // 大文件上传请参照 API 文档高级 API 上传
        File localFile = null;
        try {
            // 获取字符串的MD5结果，file.getBytes()--输入的字符串转换成字节数组
            String md5 = MD5.getMessageDigest(file.getBytes());
            String filePath = String.format("%1$s/%2$s%3$s", dir, md5, fileOriginName);
            localFile = File.createTempFile("temp", null);
            file.transferTo(localFile);
            // 指定要上传到 COS 上的路径
            String key = filePath;
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, key, localFile);
            PutObjectResult putObjectResult = cosclient.putObject(putObjectRequest);
            String qCloudDomain = cloudStorageConfig.getAddress();
            url = String.format("%1$s/%2$s", qCloudDomain, filePath);

        } catch (IOException e) {

        } finally {
            // 关闭客户端(关闭后台线程)
            cosclient.shutdown();
        }

        return url;
    }


    /**
     * 初始化CosClient相关配置， appid、accessKey、secretKey、region
     *
     * @return
     */
    public static COSClient getCosClient() {

        return null;
    }


}
