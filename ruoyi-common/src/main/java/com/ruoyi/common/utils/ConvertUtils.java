package com.ruoyi.common.utils;

import java.util.ArrayList;
import java.util.List;

public class ConvertUtils {
    public static Long[] StringArray2LongArray(String[] stringArray) {
        List<Long> list = new ArrayList<>();
        for (String str : stringArray) {
            try {
                list.add(Long.parseLong(str));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        Long[] longArray = list.toArray(new Long[list.size()]);
        return longArray;
    }
    public static String longToString(String longArray[]) {
        if (longArray == null || longArray.length < 1) {
            return null;
        }
        StringBuffer  stringArray =new StringBuffer();
        for (int i = 0; i < longArray.length; i++ ) {
            try {
                stringArray.append(longArray[i]).append(",") ;
            } catch (NumberFormatException e) {
                continue;
            }
        }
        return stringArray.substring(0,stringArray.length()-1);
    }
}
