/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:MessageUtils.java
 * Date:2020/09/13 08:43:13
 */

package com.ruoyi.common.utils;

import com.ruoyi.common.utils.spring.SpringUtils;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

/**
 * 获取i18n资源文件
 *
 * @author ruoyi
 */
public class MessageUtils {
    /**
     * 根据消息键和参数 获取消息 委托给spring messageSource
     *
     * @param code 消息键
     * @param args 参数
     * @return 获取国际化翻译值
     */
    public static String message(String code, Object... args) {
        MessageSource messageSource = SpringUtils.getBean(MessageSource.class);
        // System.out.println("MessageUtils==" + LocaleContextHolder.getLocale());
        return messageSource.getMessage(code, args, LocaleContextHolder.getLocale());
    }
}
