/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:LogUtils.java
 * Date:2020/09/13 08:43:13
 */

package com.ruoyi.common.utils;

/**
 * 处理并记录日志文件
 *
 * @author ruoyi
 */
public class LogUtils {
    public static String getBlock(Object msg) {
        if (msg == null) {
            msg = "";
        }
        return "[" + msg.toString() + "]";
    }
}
