/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:RefundResponse.java
 * Date:2020/11/27 16:26:27
 */

package com.ruoyi.common.utils.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created by 魔金商城 on 18/02/07.
 * 提现返回实体
 */
@Data
@ApiModel(description = "提现返回实体")
public class RefundResponse {

    /**
     * 返回code
     * 10000 成功
     * "10001","releaseMoney fail due to no checkPayParams fail....
     * "10002","发放金额成功，更新状态失败"
     * "10003","发放金额失败"
     * "10004","支付方式不存在"
     * "10005","交易记录不存在"
     */
    @ApiModelProperty(value = "返回code")
    private String code;
    /**
     * msg Success
     */
    @ApiModelProperty(value = "msg")
    private String msg;

    private RefundResponse() {

    }

    /**
     * 构造提现返回实体
     *
     * @param code    返回code
     * @param subCode 业务返回code
     * @return 返回提现返回实体
     */
    public static RefundResponse build(String code, String msg) {
        RefundResponse withdrawResponse = new RefundResponse();
        withdrawResponse.code = code;
        withdrawResponse.msg = msg;
        return withdrawResponse;
    }

    /**
     * 构造系统异常实体
     *
     * @return 返回提现相应实体
     */
    public static RefundResponse buildSystemError() {
        RefundResponse withdrawResponse = new RefundResponse();
        withdrawResponse.code = "-10001";
        return withdrawResponse;
    }

    /**
     * 是否提现成功
     *
     * @return 成功返回true
     */
    public boolean isSuccess() {
        return "10000".equals(this.code);
    }
}
