package com.ruoyi.common.utils;

import com.alibaba.fastjson.JSON;

/**
 * @author mky
 * @Description
 * @date 2018/7/19 00:23
 * @lastModifier
 */
public class JSONUtils {

    public static <T> T fromJson(String json, Class<T> clazz) {
        return JSON.parseObject(json, clazz);
    }

    public static String toJson(Object obj) {
        return JSON.toJSONString(obj);
    }
}
