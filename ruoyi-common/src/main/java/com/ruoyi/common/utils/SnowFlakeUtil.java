package com.ruoyi.common.utils;


/**
 * @Author: Robin
 * @Description:
 * @Date: create in 2020/9/6 0006 21:44
 */
public class SnowFlakeUtil {
    /**
     * 派号器workid：0~31
     * 机房datacenterid：0~31
     */
    private static Snowflake snowflake = new Snowflake(1, 1);

    public static Long nextId() {
        return snowflake.nextId();
    }
}
