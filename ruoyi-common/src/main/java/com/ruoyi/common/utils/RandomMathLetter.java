/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:RandomMathLetter.java
 * Date:2020/09/13 08:43:13
 */

package com.ruoyi.common.utils;

import java.util.Random;

/**
 * 随机生成字母和数字组合
 *
 * @author 魔金商城 on 2017/6/1.
 */
public class RandomMathLetter {
    /**
     * 产生随机字符串
     */
    private static Random randGen = null;
    /**
     * 数字和字母
     */
    private static char[] numbersAndLetters = null;

    /**
     * 字符串
     */
    public static String randomString(int length) {
        if (length < 1) {
            return null;
        }
        if (randGen == null) {
            randGen = new Random();
            numbersAndLetters = ("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ").toCharArray();
        }
        char[] randBuffer = new char[length];
        for (int i = 0; i < randBuffer.length; i++) {
            randBuffer[i] = numbersAndLetters[randGen.nextInt(33)];
        }
        return new String(randBuffer);
    }

    public static String randomInt(int length) {
        if (length < 1) {
            return null;
        }
        char[] numbersAndLetters = ("012345678901234567890123456789789789").toCharArray();
        if (randGen == null) {
            randGen = new Random();

        }
        char[] randBuffer = new char[length];
        for (int i = 0; i < randBuffer.length; i++) {
            randBuffer[i] = numbersAndLetters[randGen.nextInt(30)];
        }
        return new String(randBuffer);
    }

    // 产生随机电话号码格式数字
    public static String randomPhone() {
        String phone = "1";

        Random random = new Random();
        int nextInt = random.nextInt(3);

        if (nextInt == 0) {
            phone = phone + "3" + RandomMathLetter.randomNumber();
        } else if (nextInt == 1) {
            phone = phone + "5" + RandomMathLetter.randomNumber();
        } else {
            phone = phone + "8" + RandomMathLetter.randomNumber();
        }
        return phone;
    }

    // 生成长度为9的随机数
    public static String randomNumber() {

        Random random = new Random();
        int nextInt = random.nextInt(900000000) + 100000000;
        int abs = Math.abs(nextInt);
        String valueOf = String.valueOf(abs);
        return valueOf;
    }


}
