package com.ruoyi.common.config;

public interface WechatConfig {

    interface MINI {
        String APP_ID = "wx6007249d6b2348ea";
        String APP_SECRET = "1b1c5e732f38a03c980061855e68a19a";

        String MCH_ID = "1487327342";
        String MCH_KEY = "wJlIo09cibswnaghisLiI9865gJ76lJy";
    }

    interface OSS {
        String accessKeyId = "LTAI4Fw1poiMmZUoHPX3K9AR";
        String accessKeySecret = "ope9CjBZsGbJwSSygth87tHvdQ1vsd";
        String bucketName = "yangtianhe-shenzheng";
        String url = "oss.yth-yx.com";
        String endpoint = "oss-cn-shenzhen-internal.aliyuncs.com";
        String bucketUrl = "yangtianhe-shenzheng.oss-cn-shenzhen-internal.aliyuncs.com";
    }

    interface API {
        // 根据js_code 获取session_key、openid、unionid
        String JS_CODE_2_SESSION = "https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code";
        // 全局唯一接口调用凭据
        String ACCESS_TOKEN = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s";
        // 获取小程序二维码 接口B：适用于需要的码数量极多的业务场景
        String WXACODE_UNLIMIT = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=%s";
        // 统一下单
        String UNIFIED_ORDER = "https://api.mch.weixin.qq.com/pay/unifiedorder";
        // 查询订单
        String ORDER_QUERY = "https://api.mch.weixin.qq.com/pay/orderquery";
        // 申请退款
        String REFUND = "https://api.mch.weixin.qq.com/secapi/pay/refund";
        // 查询退款
        String REFUND_QUERY = "https://api.mch.weixin.qq.com/pay/refundquery";
    }

}
