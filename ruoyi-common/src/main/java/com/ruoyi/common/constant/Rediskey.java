/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:Rediskey.java
 * Date:2020/11/27 16:26:27
 */

package com.ruoyi.common.constant;

public class Rediskey {
    //快递信息缓存
    public static final String LOGISTICS_KEY = "logistics_";
    public static final String SpuDetail = "SpuDetail:%s";
    public static String TOKEN = "TOKEN:%s";
    /**
     * 用户当日兑换的步数
     */
    public static String USERED_STEP = "USERED_STEP:%s";

}
