package com.ruoyi.common.core.domain;

import com.alibaba.fastjson.JSONObject;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by troy on 02/03/2017.
 */
public class ApiRequest {
    public final static String MD5_TAIL = "7aeeb7da08390a43f73f97e3bc319c79";

    public final static String TIMESTAMP = "timestamp";
    public final static String PLATFORM = "platform";
    public final static String TOKEN = "token";
    public final static String SESSION_KEY = "sessionKey";
    public final static String APP_VERSIONS = "appVersion";
    public final static String API_VERSIONS = "apiVersions";
    public final static String DEVICE_CODE = "deviceCode";
    public final static String DATA = "data";

    public final static int LENGTH_TIMESTAMP = 10; // 时间戳的最大长度
    public final static int LENGTH_PLATFORM = 10;// XX.XX.XX, 理论上<=7, 但可能存在子版本
    public final static int LENGTH_TOKEN = 128;// token的最大长度
    public final static int LENGTH_SESSION_KEY = 128;// sessionKey的最大长度
    HttpServletRequest request;
    private long timestamp; // 10位时间戳
    private String title; // 请求接口标题
    ; // 当前时间戳
    private long currTime = System.currentTimeMillis();
    private int platform; // 平台标识(1.Android, 2.IOS, 3.WinPhone)
    private String token;// MD5验证串
    private String sessionKey; // 当前登陆sessionKey, 登陆时由借口返回, 如果没有则留空
    private String appVersion; // app版本号,格式x.x.x
    private String apiVersions; // 接口版本号,格式x.x.x
    private String deviceCode; // 设备标示
    private String data;// 接口请求参数
    private Long miniUserId;// 用户ID

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getCurrTime() {
        return currTime;
    }

    public void setCurrTime(long currTime) {
        this.currTime = currTime;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getApiVersions() {
        return apiVersions;
    }

    public void setApiVersions(String apiVersions) {
        this.apiVersions = apiVersions;
    }

    public String getDeviceCode() {
        return deviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getPlatform() {
        return platform;
    }

    public void setPlatform(int platform) {
        this.platform = platform;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Long getMiniUserId() {
        return miniUserId;
    }

    public void setMiniUserId(Long miniUserId) {
        this.miniUserId = miniUserId;
    }

    public JSONObject getJSONObject() {
        return JSONObject.parseObject(this.getData());
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ApiRequest{");
        sb.append("timestamp=").append(timestamp);
        sb.append(", platform=").append(platform);
        sb.append(", token='").append(token).append('\'');
        sb.append(", sessionKey='").append(sessionKey).append('\'');
        sb.append(", appVersion='").append(appVersion).append('\'');
        sb.append(", apiVersions='").append(apiVersions).append('\'');
        sb.append(", deviceCode='").append(deviceCode).append('\'');
        sb.append(", data='").append(data).append('\'');
        sb.append(", miniUserId='").append(miniUserId).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
